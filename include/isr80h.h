#ifndef ISR80H_H_
#define ISR80H_H_

struct interrupt_frame;

enum sys_command {
    SYS_COMMAND_SUM = 0,
    SYS_COMMAND_PRINT = 1,
    SYS_COMMAND_GET_KEY = 2,
    SYS_COMMAND_PUTC = 3,
    SYS_COMMAND_MALLOC = 4,
    SYS_COMMAND_FREE  = 5,
    SYS_COMMAND_CREATE_PROCESS = 6,
    SYS_COMMAND_INVOKE_SYSTEM_COMMAND = 7,
    SYS_COMMAND_GET_PROGRAM_ARGUMENTS = 8,
    SYS_COMMAND_EXIT= 9,
};

void isr80h_register_commands(void);

#endif
