#ifndef KHEAP_H_
#define KHEAP_H_

#include <stddef.h>

void kheap_init(void);

void *kmalloc(size_t size);

void *kzalloc(size_t size);

void kfree(void *ptr);

#endif
