#ifndef TASK_H_
#define TASK_H_

#include <stdint.h>
#include "paging.h"

#define PEACHOS_USER_PROGRAM_STACK_SIZE (1024 * 16)
#define PEACHOS_PROGRAM_VIRTUAL_ADDRESS 0x400000

#define PEACHOS_PROGRAM_VIRTUAL_STACK_ADDRESS_START 0x3ff000
#define PEACHOS_PROGRAM_VIRTUAL_STACK_ADDRESS_END  \
    (PEACHOS_PROGRAM_VIRTUAL_STACK_ADDRESS_START - \
     PEACHOS_USER_PROGRAM_STACK_SIZE)

struct registers {
    uint32_t edi;
    uint32_t esi;
    uint32_t ebp;
    uint32_t ebx;
    uint32_t edx;
    uint32_t ecx;
    uint32_t eax;

    uint32_t ip;
    uint32_t cs;
    uint32_t flags;
    uint32_t esp;
    uint32_t ss;
};

struct process;
struct interrupt_frame;

struct task {
    struct paging_4gb_chunk *page_directory;
    struct registers registers;

    struct process *process;

    struct task *task_next;
    struct task *task_prev;
};

struct task *task_new(struct process *p);

void task_free(struct task *task);

struct task *task_get_current(void);

struct task *task_get_next(void);

int32_t task_switch(struct task *task);

void task_return(struct registers *regs);

void restore_general_purpose_registers(struct registers *regs);

int32_t task_page(void);

int32_t task_page_task(const struct task *task);

void user_registers(void);

void task_run_first_ever_task(void);

void task_current_save_state(const struct interrupt_frame *frame);

int32_t copy_str_from_task(struct task *task, void *virt, void *phys,
                           uint32_t max);

void *task_get_stack_item(struct task *task, uint32_t idx);

void *task_virtual_address_to_physical(struct task *task, void *vaddr);

void task_next(void);

#endif
