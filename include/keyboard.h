#ifndef KEYBOARD_H_
#define KEYBOARD_H_

#include <stdint.h>
#include <stdbool.h>

typedef int32_t (*keyboard_init_fn)(void);

struct process;

struct keyboard {
    keyboard_init_fn init;
    char name[20];
    struct keyboard *next;
    bool is_capslock_enabled;
};

void keyboard_init(void);
void keyboard_backspace(struct process *p);
void keyboard_push(char c);
char keyboard_pop(void);
void keyboard_set_capslock(struct keyboard *kb, bool is_enabled);
bool keyboard_get_capslock(struct keyboard *kb);

#endif
