#ifndef DISK_STREAMER_H_
#define DISK_STREAMER_H_

#include <stdint.h>

#include "disk.h"

struct disk_stream {
    uint64_t pos;
    struct disk *disk;
};

int32_t disk_stream_init(uint32_t disk_id, struct disk_stream *stream);

void disk_stream_seek(struct disk_stream *stream, uint64_t pos);

int32_t disk_stream_read(struct disk_stream *stream, uint32_t length,
                         uint8_t *buffer);

#endif
