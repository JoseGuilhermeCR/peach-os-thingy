#ifndef PROCESS_H_
#define PROCESS_H_

#include "path.h"
#include "task.h"

#include <stdint.h>
#include <stddef.h>

#define PEACHOS_MAX_PROGRAM_ALLOCATIONS 1024
#define PEACHOS_KEYBOARD_BUFFER_SIZE 1024

enum process_file_type {
    PROCESS_FILE_TYPE_ELF,
    PROCESS_FILE_TYPE_BINARY,
};

struct process_allocation {
    void *ptr;
    size_t size;
};

struct cmdarg {
    char arg[512];
    struct cmdarg *next;
};

struct procarg {
    int32_t argc;
    char **argv;
};

struct process {
    uint16_t id;

    char filename[PEACHOS_MAX_PATH];

    // The main process task.
    struct task *task;

    // The memory (malloc) allocations of the process.
    struct process_allocation allocations[PEACHOS_MAX_PROGRAM_ALLOCATIONS];

    enum process_file_type file_type;

    union {
        // The physical pointer to the process memory.
        void *ptr;
        struct elf_file *elf;
    };

    // The physical pointer to the stack memory.
    void *stack;

    // The size of the data pointed to by "ptr".
    uint32_t size;

    struct keyboard_buffer {
        char buffer[PEACHOS_KEYBOARD_BUFFER_SIZE];
        uint32_t tail;
        uint32_t head;
    } kb_buffer;

    struct procarg args;
};

int32_t process_switch(struct process *p);

int32_t process_load_switch(const char *filename, struct process **p);

int32_t process_load(const char *filename, struct process **process_out);

struct process *process_get_current(void);

void *process_malloc(struct process *p, size_t size);

void process_free(struct process *p, void *ptr);

void *process_invoke_system_command(const struct interrupt_frame *frame);

int32_t process_inject_arguments(struct process *p, struct cmdarg *root);

void process_get_arguments(struct process *p, int32_t *argc, char ***argv);

void process_terminate(struct process *p);

#endif
