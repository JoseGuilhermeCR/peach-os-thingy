#ifndef ELF_LOADER_H_
#define ELF_LOADER_H_

#include <stdint.h>
#include <stddef.h>

#include "elf.h"
#include "path.h"

struct elf_file {
    char filename[PEACHOS_MAX_PATH];

    uint32_t data_size;
    void *data;

    void *vaddr;
    void *vaddr_end;

    void *paddr;
    void *paddr_end;
};

int32_t elf_load(const char *filename, struct elf_file **elf_out);

void elf_close(struct elf_file *elf);

void *elf_virtual_base(struct elf_file *file);

void *elf_virtual_end(struct elf_file *file);

void *elf_phys_base(struct elf_file *file);

void *elf_phys_end(struct elf_file *file);

void *elf_phdr_phys_address(struct elf_file *elf, struct elf32_phdr *phdr);

struct elf32_shdr *elf_sheader(struct elf_header *header);

struct elf32_phdr *elf_pheader(struct elf_header *header);

struct elf_header *elf_header(struct elf_file *file);

#endif
