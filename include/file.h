#ifndef FILE_H_
#define FILE_H_

#include <stdint.h>

struct disk;
struct path_part;

enum file_seek_mode {
    FILE_SEEK_MODE_SET,
    FILE_SEEK_MODE_CUR,
    FILE_SEEK_MODE_END,
};

enum file_mode_bit {
    FILE_MODE_BIT_READ = 1 << 1,
    FILE_MODE_BIT_WRITE = 1 << 2,
    FILE_MODE_BIT_APPEND = 1 << 3,
};

enum {
    FILE_STATE_READ_ONLY = 1,
};

struct file_stat {
    uint32_t flags;
    uint32_t file_size;
};

typedef void *(*fs_open_fn)(struct disk *disk, struct path_part *path,
                            uint32_t mode);
typedef int32_t (*fs_read_fn)(struct disk *disk, void *data, uint32_t size,
                              uint32_t nmemb, char *out);
typedef int32_t (*fs_seek_fn)(void *data, uint32_t offset,
                              enum file_seek_mode whence);
typedef int32_t (*fs_fstat_fn)(void *data, struct file_stat *sb);
typedef int32_t (*fs_close_fn)(void *data);
// If the provided disk is using the file system, return 0 else negative error number.
typedef int32_t (*fs_resolve_fn)(struct disk *disk);

struct fs {
    fs_open_fn open;
    fs_resolve_fn resolve;
    fs_seek_fn seek;
    fs_read_fn read;
    fs_fstat_fn fstat;
    fs_close_fn close;
    char name[20];
};

struct file_descriptor {
    uint32_t index;
    struct fs *fs;

    // Private data for internal file descriptor.
    void *data;

    // The disk that the file descriptor should be used on.
    struct disk *disk;
};

void fs_init(void);

int32_t fs_fopen(const char *pathname, const char *mode);

int32_t fs_fstat(int32_t fd, struct file_stat *sb);

int32_t fs_fseek(int32_t fd, int32_t offset, enum file_seek_mode whence);

int32_t fs_fclose(int32_t fd);

int32_t fs_fread(void *ptr, uint32_t size, uint32_t nmemb, int32_t fd);

int32_t fs_add(struct fs *fs);

struct fs *fs_resolve(struct disk *disk);

#endif
