#ifndef PATH_H_
#define PATH_H_

#define PEACHOS_MAX_PATH 108
#define PEACHOS_PATH_SEPARATOR '/'

struct path_root {
    int drive_no;
    struct path_part *first;
};

struct path_part {
    const char *str;
    struct path_part *next;
};

struct path_root *path_parse_start(const char *pathname,
                                   const char *current_dir_path);

void path_free(struct path_root *root);

#endif
