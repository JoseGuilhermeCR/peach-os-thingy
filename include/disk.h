#ifndef DISK_H_
#define DISK_H_

#include <stdint.h>

struct fs;

enum disk_type {
    DISK_TYPE_REAL = 0,
};

struct disk {
    enum disk_type type;
    uint32_t sector_size;
    uint32_t id;

    struct fs *fs;
    void *fs_private_data;
};

void disk_search_and_init(void);

struct disk *disk_get(uint32_t idx);

int32_t disk_read_block(struct disk *disk, uint32_t lba, uint32_t len,
                        uint8_t buffer[len]);

#endif
