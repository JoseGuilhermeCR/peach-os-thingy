#ifndef IO_H_
#define IO_H_

#include <stdint.h>

uint8_t insb(uint16_t port);

uint16_t insw(uint16_t port);

void outb(uint16_t port, uint8_t byte);

void outw(uint16_t port, uint16_t word);

#endif
