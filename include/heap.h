#ifndef HEAP_H_
#define HEAP_H_

#include "config.h"
#include <stdint.h>
#include <stddef.h>

enum {
    HEAP_BLOCK_TABLE_ENTRY_FREE = 0 << 0,
    HEAP_BLOCK_TABLE_ENTRY_USED = 1 << 0,
    HEAP_BLOCK_HAS_NEXT = 1 << 7,
    HEAP_BLOCK_IS_FIRST = 1 << 6,
};

struct heap_table {
    uint8_t *entries;
    size_t entry_count;
};

struct heap {
    struct heap_table *table;
    void *saddr;
};

int32_t heap_create(struct heap *heap, void *beg, void *end,
                    struct heap_table *table);

void *heap_malloc(struct heap *heap, size_t size);

void heap_free(struct heap *heap, void *ptr);

#endif
