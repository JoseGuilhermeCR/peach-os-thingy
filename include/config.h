#ifndef CONFIG_H_
#define CONFIG_H_

#define KERNEL_CODE_SELECTOR 0x08
#define KERNEL_DATA_SELECTOR 0x10

#define PEACHOS_TOTAL_INTERRUPTS 512

#define PEACHOS_HEAP_SIZE_BYTES (100 * 1024 * 1024)
#define PEACHOS_HEAP_BLOCK_SIZE 4096
#define PEACHOS_HEAP_ADDRESS 0x1000000
// NOTE(José): OsDev shows this address is free for use, though it's rather small.
// Our table fits in there though.
// https://wiki.osdev.org/Memory_Map_(x86)
#define PEACHOS_HEAP_TABLE_ADDRESS 0x7e00

#define PEACHOS_DISK_SECTOR_SIZE_BITS 9
#define PEACHOS_DISK_SECTOR_SIZE (1 << PEACHOS_DISK_SECTOR_SIZE_BITS)

#endif
