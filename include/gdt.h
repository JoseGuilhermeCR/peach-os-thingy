#ifndef GDT_H_
#define GDT_H_

#include <stdint.h>

#define PEACHOS_TOTAL_GDT_SEGMENTS 6

struct gdt {
    uint16_t segment;
    uint16_t base_first;
    uint8_t base;
    uint8_t access;
    uint8_t high_flags;
    uint8_t base_24_31_bits;
} __attribute__((packed));

struct gdt_structured {
    uint32_t base;
    uint32_t limit;
    uint8_t type;
};

void gdt_load(struct gdt *gdt, uint32_t size);

void gdt_structured_to_gdt(struct gdt *gdt,
                           const struct gdt_structured *gdt_structured,
                           uint32_t total_entries);

#endif
