#ifndef PAGING_H_
#define PAGING_H_

#include <stdint.h>

#define PAGING_TOTAL_ENTRIES_PER_TABLE 1024
#define PAGING_TOTAL_ENTRIES_PER_DIRECTORY 1024
#define PAGING_PAGE_SIZE 4096

enum {
    PTE_PRESENT = 1 << 0,
    PTE_READ_WRITE = 1 << 1,
    PTE_USER_SUPERVISOR = 1 << 2,
    PTE_WRITE_THROUGH = 1 << 3,
    PTE_CACHE_DISABLED = 1 << 4,
    PTE_ACCESSED = 1 << 5,
    PTE_DIRTY = 1 << 6,
    PTE_ATTRIBUTE = 1 << 7,
    PTE_GLOBAL = 1 << 8,
};

#define PTE_FLAGS_MASK 0x017f

enum {
    PTD_PRESENT = 1 << 0,
    PTD_READ_WRITE = 1 << 1,
    PTD_USER_SUPERVISOR = 1 << 2,
    PTD_WRITE_THROUGH = 1 << 3,
    PTD_CACHE_DISABLED = 1 << 4,
    PTD_ACCESSED = 1 << 5,
    PTD_AVAILABLE = 1 << 6,
    PTD_PAGE_SIZE = 1 << 7,
};

// NOTE(José): The 7th bit determines the size of the page.
// Since we only have 4kb pages, the mask disables the bit.
#define PTD_FLAGS_MASK 0x7f

struct paging_4gb_chunk {
    uint32_t *dir_entry;
};

struct paging_4gb_chunk *paging_new_4gb(uint16_t table_entry_flags,
                                        uint16_t directory_entry_flags);

uint32_t *paging_4gb_chunk_get_dir(struct paging_4gb_chunk *chunk);

int32_t paging_map(const struct paging_4gb_chunk *chunk, void *vaddr,
                   void *addr, uint16_t flags);

void paging_switch(const struct paging_4gb_chunk *chunk);

void enable_paging(void);

void paging_free_4gb(struct paging_4gb_chunk *chunk);

int32_t paging_map_to(const struct paging_4gb_chunk *chunk, void *virt,
                      void *phys, void *phys_end, uint16_t flags);

int32_t paging_map_range(const struct paging_4gb_chunk *chunk, void *virt,
                         void *phys, uint32_t count, uint16_t flags);

void *paging_align_address(void *ptr);

void *paging_align_to_lower_page(void *ptr);

void paging_get(struct paging_4gb_chunk *chunk, void *vaddr, void **addr_out,
                uint16_t *flags_out);

void *paging_get_physical_addr(struct paging_4gb_chunk *chunk, void *vaddr);
#endif
