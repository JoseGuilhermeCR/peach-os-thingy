#ifndef KERNEL_H_
#define KERNEL_H_

void kernel_init(void);
void kernel_page(void);
void kernel_registers(void);

// TODO(José): REMOVE!
void print(const char *s);
void putc(char c);
void panic(const char *msg);

#define ERROR(value) (void *)(value)
#define ERROR_I(value) (int)(value)
#define ISERR(value) ((int)value < 0)

#endif
