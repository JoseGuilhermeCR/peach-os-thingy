#ifndef IDT_H_
#define IDT_H_

#include <stdint.h>

struct idt_desc {
    // Offset [15:0]
    uint16_t offset_lo;
    // GDT selector
    uint16_t selector;
    uint8_t reserved;
    // Type and attributes
    uint8_t type_attr;
    // Offset [31:16]
    uint16_t offset_hi;
} __attribute__((packed));

struct idtr_desc {
    // Descriptor Table Size - 1
    uint16_t limit;
    // Base address of interrupt table
    uint32_t base;
} __attribute__((packed));

struct interrupt_frame {
    uint32_t edi;
    uint32_t esi;
    uint32_t ebp;
    uint32_t reserved;
    uint32_t ebx;
    uint32_t edx;
    uint32_t ecx;
    uint32_t eax;
    uint32_t ip;
    uint32_t cs;
    uint32_t flags;
    uint32_t esp;
    uint32_t ss;
} __attribute__((packed));

typedef void *(*isr80h_command_fn)(const struct interrupt_frame *frame);
typedef void (*int_callback_fn)(const struct interrupt_frame *frame);

void idt_init(void);

void enable_interrupts(void);

void disable_interrupts(void);

void isr80h_register_command(int32_t command, isr80h_command_fn command_fn);

int32_t idt_register_interrupt_callback(uint32_t interrupt, int_callback_fn fn);

#endif
