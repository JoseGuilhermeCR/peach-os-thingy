section .program_start exec

extern libc_init

global _start

_start:
    call libc_init
    ret
