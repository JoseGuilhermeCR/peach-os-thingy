#include "stdio.h"
#include "syscalls.h"

#include <stdarg.h>
#include <string.h>

struct cmdarg *parse_cmd(uint32_t max, const char *command)
{
    struct cmdarg *root = NULL;
    struct cmdarg *cur = NULL;
    char cmd[1025];

    if (max >= sizeof(cmd))
        return NULL;

    strncpy(cmd, command, sizeof(cmd));

    char *tok = strtok(cmd, " ");
    if (!tok)
        goto out;

    root = malloc(sizeof(*root));
    if (!root)
        goto out;

    strncpy(root->arg, tok, sizeof(root->arg));
    root->next = NULL;

    cur = root;
    tok = strtok(NULL, " ");

    while (tok) {
        struct cmdarg *new = malloc(sizeof(*new));
        if (!new)
            break;

        strncpy(new->arg, tok, sizeof(new->arg));
        new->next = NULL;
        cur->next = new;
        cur = new;
        tok = strtok(NULL, " ");
    }

out:
    return root;
}

char *itoa(int32_t i)
{
    static char text[12];

    int32_t loc = 11;
    text[11] = '\0';

    char neg = 1;
    if (i >= 0) {
        neg = 0;
        i = -i;
    }

    while (i) {
        text[--loc] = '0' - (i % 10);
        i /= 10;
    }

    if (loc == 11)
        text[--loc] = '0';

    if (neg)
        text[--loc] = '-';

    return &text[loc];
}

int32_t printf(const char *format, ...)
{
    va_list ap;

    const char *p, *sval;
    int32_t ival;
    char cval;

    va_start(ap, format);
    for (p = format; *p; ++p) {
        if (*p != '%') {
            putc(*p);
            continue;
        }

        switch (*++p) {
        case 'i':
            ival = va_arg(ap, int32_t);
            print(itoa(ival));
            break;
        case 's':
            sval = va_arg(ap, const char *);
            print(sval);
            break;
        case 'c':
            cval = va_arg(ap, int32_t);
            putc(cval);
            break;
        default:
            putc(*p);
            break;
        }
    }
    va_end(ap);

    return 0;
}

char getkey_block(void)
{
    char c;

    do {
        c = getkey();
    } while (c == '\0');

    return c;
}

void terminal_readline(size_t size, char *out, bool should_output_mid_typing)
{
    size_t i = 0;
    for (; i < size; ++i) {
        const char c = getkey_block();

        if (c == '\r')
            break;

        if (should_output_mid_typing)
            putc(c);

        if (c == '\b' && i > 1) {
            out[i - 1] = '\0';
            i -= 2;
            continue;
        }

        out[i] = c;
    }

    out[i] = '\0';
}

void invoke_sys_run(const char *command)
{
    char buf[1024];
    strncpy(buf, command, sizeof(buf));
    struct cmdarg *root = parse_cmd(sizeof(buf), buf);
    if (!root)
        return;
    invoke_sys_command(root);
}
