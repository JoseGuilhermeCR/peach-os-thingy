section .asm exec

global print:function
print:
    push ebp
    mov ebp, esp

    push dword [ebp + 8]
    mov eax, 1 ; command print
    int 0x80
    add esp, 4

    pop ebp
    ret

global getkey:function
getkey:
    push ebp
    mov ebp, esp

    mov eax, 2
    int 0x80 ; command getkey

    pop ebp
    ret

global putc:function
putc
    push ebp
    mov ebp, esp

    push dword [ebp + 8]
    mov eax, 3 ; commad putc
    int 0x80
    add esp, 4

    pop ebp
    ret

global malloc:function
malloc:
    push ebp
    mov ebp, esp

    push dword [ebp + 8]
    mov eax, 4 ; command malloc
    int 0x80
    add esp, 4

    pop ebp
    ret

global free:function
free:
    push ebp
    mov ebp, esp

    push dword [ebp + 8]
    mov eax, 5 ; command free
    int 0x80
    add esp, 4

    pop ebp
    ret

global create_process:function
create_process:
    push ebp
    mov ebp, esp

    push dword [ebp + 8]
    mov eax, 6 ; command create process
    int 0x80
    add esp, 4

    pop ebp
    ret

global invoke_sys_command:function
invoke_sys_command:
    push ebp
    mov ebp, esp

    push dword [ebp + 8]
    mov eax, 7
    int 0x80
    add esp, 4

    pop ebp
    ret

global get_process_arguments:function
get_process_arguments:
    push ebp
    mov ebp, esp

    push dword [ebp + 8]
    mov eax, 8
    int 0x80
    add esp, 4

    pop ebp
    ret

global exit:function
exit:
    push ebp
    mov ebp, esp

    mov eax, 9
    int 0x80
    add esp, 4

    pop ebp
    ret

