#include "syscalls.h"

#include <stdint.h>

extern int32_t main(int32_t argc, const char **argv);

int32_t libc_init(void)
{
    struct procarg args;
    get_process_arguments(&args);
    main(args.argc, args.argv);
    exit();

}
