#ifndef STDIO_H_
#define STDIO_H_

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

struct cmdarg {
    char arg[512];
    struct cmdarg *next;
};

char *itoa(int32_t i);

char getkey_block(void);

int32_t printf(const char *format, ...);

void terminal_readline(size_t size, char *out, bool should_output_mid_typing);

struct cmdarg *parse_cmd(uint32_t max, const char *command);

#endif
