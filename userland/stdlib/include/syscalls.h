#ifndef SYSCALLS_H_
#define SYSCALLS_H_

#include <stddef.h>
#include <stdint.h>

struct procarg {
    int32_t argc;
    char **argv;
};

struct cmdarg;

void print(const char *message);

char getkey(void);

void putc(char c);

void *malloc(size_t size);

void free(void *ptr);

void create_process(const char *filename);

void invoke_sys_command(struct cmdarg *args);

void invoke_sys_run(const char *command);

void get_process_arguments(struct procarg *args);

__attribute__((noreturn)) void exit(void);

#endif
