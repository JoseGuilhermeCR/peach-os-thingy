#ifndef STRING_H_
#define STRING_H_

#include <stddef.h>
#include <stdint.h>

char tolower(char c);

int32_t istrncmp(const char *s1, const char *s2, int32_t n);

size_t strnlen_terminator(const char *str, size_t size, char terminator);

size_t strlen(const char *str);

size_t strnlen_terminator(const char *str, size_t size, char terminator);

void *memset(void *ptr, int c, size_t size);

int32_t memcmp(const void *s1, const void *s2, size_t size);

void *memcpy(void *dest, void *src, size_t size);

char *strncpy(char *dest, const char *src, size_t size);

char *strcpy(char *dest, const char *src);

char *strtok(char *str, const char *delimiters);

#endif
