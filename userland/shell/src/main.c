#include <stdio.h>
#include <stdint.h>
#include <stddef.h>

#include "syscalls.h"

int32_t main(int32_t argc, const char **argv)
{
    (void)argc;
    (void)argv;

    print("shell loaded\n");

    char buffer[1024];
    while (true) {
        print("$ ");
        terminal_readline(sizeof(buffer), buffer, true);
        invoke_sys_run(buffer);
        putc('\n');
    }

    return 0;
}
