#include <stdint.h>

#include "syscalls.h"
#include "stdio.h"
#include "string.h"

int main(int32_t argc, const char **argv)
{
    putc('\n');

    while (1) {
        for (int32_t i = 0; i < argc; ++i) {
            printf("%i: %s ", i, argv[i]);
        }
    }

    return 0;
}
