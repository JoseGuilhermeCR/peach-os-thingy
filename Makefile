# NOTE(José): Generation of subdirectories
# and separation of source and object files
# are heavily inspired on content of:
# https://stackoverflow.com/questions/58951332/using-suffix-rules-in-makefile/58953668#58953668

OUT_DIR = build
OUT_BOOT = boot.bin
OUT_KERNEL = kernel
OUT_OS = os.bin

BOOT_AS_SRC = src/boot/boot.asm

KERNEL_C_SRC = src/kernel.c \
		src/idt.c \
		src/heap.c \
		src/kheap.c \
		src/paging.c \
		src/disk.c \
		src/string.c \
		src/path.c \
		src/disk_stream.c \
		src/file.c \
		src/fat16.c \
		src/gdt.c \
		src/task.c \
		src/process.c \
		src/isr80h.c \
		src/keyboard.c \
		src/ps2_keyboard.c \
		src/elf.c \
		src/elf_loader.c

KERNEL_AS_SRC = src/asm/kernel.asm \
		 src/asm/idt.asm \
		 src/asm/io.asm \
		 src/asm/paging.asm \
		 src/asm/gdt.asm \
		 src/asm/tss.asm \
		 src/asm/task.asm

# NOTE(José): Prepend .{c,asm} files with $(OUT_DIR) and change their
# extension to .{co,asmo} indicating that they are object files.
# Basically, get the list of object files we need to build.
KERNEL_C_OBJ = $(patsubst %.c,$(OUT_DIR)/%.co,$(KERNEL_C_SRC))
KERNEL_AS_OBJ = $(patsubst %.asm,$(OUT_DIR)/%.asmo,$(KERNEL_AS_SRC))

# NOTE(José): Generate list of dependencies for every object file by changing
# their extensions to .d.
KERNEL_DEP = $(patsubst %.co,%.d,$(KERNEL_C_OBJ))
KERNEL_DEP += $(patsubst %.asmo,%.d,$(KERNEL_AS_OBJ))

PREFIX = i686-elf-

CC = $(PREFIX)gcc
OBJCOPY = $(PREFIX)objcopy
AS = nasm

KERNEL_INCLUDES = -Iinclude

KERNEL_CFLAGS = -ffreestanding -fno-builtin -fno-omit-frame-pointer \
		-Wall -Wextra -Wshadow \
		-std=gnu17 -MMD $(KERNEL_INCLUDES)

KERNEL_LDFLAGS = -nostartfiles -nostdlib -nodefaultlibs \
		 -lgcc -Tlinker.ld

KERNEL_ASFLAGS = -f elf

BOOT_ASFLAGS = -f bin

ifeq ($(DEBUG),1)
BOOT_ASFLAGS += -g
KERNEL_ASFLAGS += -g
KERNEL_CFLAGS += -g3 -O0
KERNEL_LDFLAGS += -g3 -O0
endif

# NOTE(José): Needed for the $$(dir $$@) syntax used below.
# TODO(José): Find out how it works and understand it's usage.
.SECONDEXPANSION :
.SECONDARY :

all : $(OUT_DIR)/$(OUT_OS) | $(OUT_DIR)/

# NOTE(José): Create other directories in the build tree.
$(OUT_DIR)/%/ : | $(OUT_DIR)/
	while ! mkdir -p $@; do true; done

# NOTE(José): Create root directory.
$(OUT_DIR)/ :
	mkdir -p $@

# NOTE(José): Do not regenerate any .d file.
# They are only generated during builds.
$(OUT_DIR)/%.d : ;

$(OUT_DIR)/$(OUT_OS) : $(OUT_DIR)/$(OUT_KERNEL) $(OUT_DIR)/$(OUT_BOOT) userland | $(OUT_DIR)/
	# NOTE(José): Extract the raw binary from the ELF.
	$(OBJCOPY) -O binary $(OUT_DIR)/$(OUT_KERNEL) $(OUT_DIR)/$(OUT_KERNEL).bin
	# NOTE(José): Copy the bootloader to the OS binary file.
	dd if=$(OUT_DIR)/$(OUT_BOOT) of=$(OUT_DIR)/$(OUT_OS)
	# NOTE(José): Copy the kernel to the OS binary file.
	dd if=$(OUT_DIR)/$(OUT_KERNEL).bin of=$(OUT_DIR)/$(OUT_OS) oflag=append conv=notrunc
	# NOTE(José): Add some extra space used by the file system.
	dd if=/dev/zero bs=1M count=16 of=$(OUT_DIR)/$(OUT_OS) oflag=append conv=notrunc
	# NOTE(José): Copy a single file to the file system.
	mkdir -p os_mount
	sudo mount -t vfat -o gid=$(USER),uid=$(USER) $(OUT_DIR)/$(OUT_OS) os_mount
	cp hello.txt os_mount
	cp userland/blank/build/blank os_mount
	cp userland/shell/build/shell os_mount
	sudo umount os_mount
	rm -r os_mount

$(OUT_DIR)/$(OUT_BOOT) : $(BOOT_AS_SRC) | $(OUT_DIR)/
	$(AS) $(BOOT_ASFLAGS) $^ -o $@

$(OUT_DIR)/$(OUT_KERNEL) : $(KERNEL_C_OBJ) $(KERNEL_AS_OBJ) | $(OUT_DIR)/
	$(CC) $(KERNEL_LDFLAGS) $^ -o $@

# TODO(José): Understand usage of second expansion to create the needed subdirectories.
$(OUT_DIR)/%.asmo : %.asm | $$(dir $$@)
	$(AS) $(KERNEL_ASFLAGS) -MD $@.d $< -o $@

# TODO(José): Understand usage of second expansion to create the needed subdirectories.
$(OUT_DIR)/%.co : %.c | $$(dir $$@)
	$(CC) $(KERNEL_CFLAGS) -c $< -o $@

-include $(KERNEL_DEP)

.PHONY : clean
clean : clean_userland
	rm -rf $(OUT_DIR)

.PHONY : debug
debug : all
	gdb -ex 'file $(OUT_DIR)/$(OUT_KERNEL)' -ex 'break kernel_init' -ex 'target remote | qemu-system-i386 -S -gdb stdio -hda $(OUT_DIR)/$(OUT_OS)'

.PHONY : run
run : all
	qemu-system-i386 -hda $(OUT_DIR)/$(OUT_OS)

.PHONY : mount
mount : all
	mkdir -p os_mount
	sudo mount -t vfat -o gid=$(USER),uid=$(USER) $(OUT_DIR)/$(OUT_OS) os_mount

.PHONY : userland
userland :
	cd userland && $(MAKE) all

.PHONY : clean_userland
clean_userland :
	cd userland && $(MAKE) clean
