#include "idt.h"
#include "io.h"
#include "keyboard.h"

#define PS2_PORT 0x64
#define PS2_COMMAND_ENABLE_FIRST_PORT 0xae
#define PS2_KEY_RELEASED 0x80
#define PS2_KEYBOARD_INTERRUPT 0x21
#define PS2_KEYBOARD_INPUT_PORT 0x60
#define PS2_CAPSLOCK 0x3a

static const uint8_t scan_set_one[] = {
    0x00, 0x1b, '1',  '2',  '3',  '4',  '5',  '6',  '7',  '8',  '9',  '0',
    '-',  '=',  0x08, '\t', 'Q',  'W',  'E',  'R',  'T',  'Y',  'U',  'I',
    'O',  'P',  '[',  ']',  0x0d, 0x00, 'A',  'S',  'D',  'F',  'G',  'H',
    'J',  'K',  'L',  ';',  '\'', '`',  0x00, '\\', 'Z',  'X',  'C',  'V',
    'B',  'N',  'M',  ',',  '.',  '/',  0x00, '*',  0x00, 0x20, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, '7',  '8',  '9',
    '-',  '4',  '5',  '6',  '+',  '1',  '2',  '3',  '0',  '.'
};

static int32_t ps2_kb_init(void);

static struct keyboard ps2_kb = {
    .name = "PS2",
    .init = ps2_kb_init,
};

static uint8_t ps2_kb_scancode_to_char(uint8_t scancode)
{
    if (scancode > sizeof(scan_set_one))
        return 0;

    char c = scan_set_one[scancode];
    if (!keyboard_get_capslock(&ps2_kb)) {
        if (c >= 'A' && c <= 'Z')
            c += 32;
    }

    return c;
}

static void ps2_kb_handle_interrupt(const struct interrupt_frame *frame)
{
    (void)frame;

    // kernel_page();

    const uint8_t scancode = insb(PS2_KEYBOARD_INPUT_PORT);
    // NOTE(José): Ignore next byte.
    insb(PS2_KEYBOARD_INPUT_PORT);

    if (scancode & PS2_KEY_RELEASED)
        return;

    if (scancode == PS2_CAPSLOCK)
        keyboard_set_capslock(&ps2_kb, !keyboard_get_capslock(&ps2_kb));

    const uint8_t c = ps2_kb_scancode_to_char(scancode);
    if (c)
        keyboard_push(c);

    // task_page();
}

static int32_t ps2_kb_init(void)
{
    idt_register_interrupt_callback(PS2_KEYBOARD_INTERRUPT,
                                    ps2_kb_handle_interrupt);

    keyboard_set_capslock(&ps2_kb, false);

    outb(PS2_PORT, PS2_COMMAND_ENABLE_FIRST_PORT);
    return 0;
}

struct keyboard *ps2_keyboard_init(void)
{
    return &ps2_kb;
}
