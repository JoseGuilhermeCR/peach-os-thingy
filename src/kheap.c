#include "kheap.h"
#include "heap.h"
#include "config.h"
#include "string.h"

// TODO(José): Remove from here, only for print.
#include "kernel.h"

static struct heap kernel_heap;
static struct heap_table kernel_heap_table;

void kheap_init(void)
{
    const uint32_t total_table_entries =
        PEACHOS_HEAP_SIZE_BYTES / PEACHOS_HEAP_BLOCK_SIZE;

    // NOTE(José): I don't really like  the idea of pointing to a rather
    // random address. I'd rather leave it at the .bss section.
    kernel_heap_table.entries = (uint8_t *)PEACHOS_HEAP_TABLE_ADDRESS;
    kernel_heap_table.entry_count = total_table_entries;

    void *beg = (void *)PEACHOS_HEAP_ADDRESS;
    void *end = beg + PEACHOS_HEAP_SIZE_BYTES;

    const int32_t ret = heap_create(&kernel_heap, beg, end, &kernel_heap_table);
    if (ret < 0)
        print("Failed to create heap\n");
}

void *kmalloc(size_t size)
{
    return heap_malloc(&kernel_heap, size);
}

void *kzalloc(size_t size)
{
    void *ptr = kmalloc(size);
    if (!ptr)
        return NULL;
    memset(ptr, 0, size);
    return ptr;
}

void kfree(void *ptr)
{
    heap_free(&kernel_heap, ptr);
}
