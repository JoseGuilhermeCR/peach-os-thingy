#include "disk.h"

#include "config.h"
#include "io.h"
#include "status.h"
#include "file.h"

#include <stddef.h>

#define LBA_BITS 28
#define SECTOR_WORDS 256

#define ATA_STATUS_DRQ (1 << 3)

enum drive { DRIVE_MASTER = 0xe0 };

// NOTE(José): Registers of the primary bus.
enum ata_io_reg {
    ATA_IO_REG_DATA = 0x1f0,
    ATA_IO_REG_ERROR_AND_FEATURES,
    ATA_IO_REG_SECTOR_COUNT,
    ATA_IO_REG_LBA_LO,
    ATA_IO_REG_LBA_MID,
    ATA_IO_REG_LBA_HI,
    ATA_IO_REG_DRIVE,
    ATA_IO_REG_COMMAND_AND_STATUS,
};

enum command {
    COMMAND_READ_SECTORS = 0x20,
};

static struct disk primary_disk = {
    .type = DISK_TYPE_REAL,
    .sector_size = PEACHOS_DISK_SECTOR_SIZE,
    .id = 0,
};

static void send_drive(enum drive d, uint32_t lba)
{
    // TODO(José): Assert.
    // assert(!(lba >> 28))
    outb(ATA_IO_REG_DRIVE, (uint8_t)d | (lba >> 24));
}

static void send_count(uint8_t count)
{
    outb(ATA_IO_REG_SECTOR_COUNT, count);
}

static void send_lba(uint32_t lba)
{
    // TODO(José): Assert.
    // assert(!(lba >> 28))

    // NOTE(José): There are 28 bits, remember that the upper
    // 4 were sent together with the drive! So here we send
    // the other 24.
    outb(ATA_IO_REG_LBA_LO, lba & 0x000ff);
    outb(ATA_IO_REG_LBA_MID, (lba & 0x0ff00) >> 8);
    outb(ATA_IO_REG_LBA_HI, (lba & 0xf0000) >> 16);
}

static void send_command(enum command cmd)
{
    outb(ATA_IO_REG_COMMAND_AND_STATUS, (uint8_t)cmd);
}

static uint8_t read_status(void)
{
    return insb(ATA_IO_REG_COMMAND_AND_STATUS);
}

static uint16_t read_word(void)
{
    return insw(ATA_IO_REG_DATA);
}

// NOTE(José):
// https://wiki.osdev.org/ATA_PIO_Mode
// 28-bit PIO
static int32_t disk_read_sector(uint32_t lba, uint32_t len, uint8_t buffer[len])
{
    if (lba >> LBA_BITS)
        return -EINVARG;

    if (len > UINT8_MAX)
        return -EINVARG;

    send_drive(DRIVE_MASTER, lba);
    send_count(len);
    send_lba(lba);
    send_command(COMMAND_READ_SECTORS);

    uint16_t *ptr = (uint16_t *)buffer;
    for (uint32_t i = 0; i < len; ++i) {
        while (!(read_status() & ATA_STATUS_DRQ))
            ;

        for (uint32_t j = 0; j < SECTOR_WORDS; ++j)
            *ptr++ = read_word();
    }

    return 0;
}

void disk_search_and_init(void)
{
    primary_disk.fs = fs_resolve(&primary_disk);
    return;
}

struct disk *disk_get(uint32_t idx)
{
    if (idx > 0)
        return NULL;

    return &primary_disk;
}

int32_t disk_read_block(struct disk *disk, uint32_t lba, uint32_t len,
                        uint8_t buffer[len])
{
    if (disk != &primary_disk)
        return -EIO;

    return disk_read_sector(lba, len, buffer);
}
