#include "process.h"
#include "elf_loader.h"
#include "file.h"
#include "kernel.h"
#include "kheap.h"
#include "paging.h"
#include "status.h"
#include "string.h"

#include "task.h"
#include <stdbool.h>

#define PEACHOS_MAX_PROCESSES 12

static struct process *current_process;

static struct process *processes[PEACHOS_MAX_PROCESSES];

static void process_init(struct process *p)
{
    memset(p, 0, sizeof(*p));
}

struct process *process_get_current(void)
{
    return current_process;
}

int32_t process_switch(struct process *p)
{
    current_process = p;
    return 0;
}

static int32_t process_find_free_allocation_index(struct process *p)
{
    int32_t res = -ENOMEM;

    for (int32_t i = 0; i < PEACHOS_MAX_PROGRAM_ALLOCATIONS; ++i) {
        if (!p->allocations[i].ptr) {
            res = i;
            break;
        }
    }

    return res;
}

void *process_malloc(struct process *p, size_t size)
{
    int32_t idx = process_find_free_allocation_index(p);
    if (idx < 0)
        return NULL;

    void *ptr = kmalloc(size);
    if (!ptr)
        return NULL;

    int32_t ret = paging_map_to(
        p->task->page_directory, ptr, ptr, paging_align_address(ptr + size),
        PTE_PRESENT | PTE_READ_WRITE | PTE_USER_SUPERVISOR);
    if (ret != 0) {
        kfree(ptr);
        return NULL;
    }

    p->allocations[idx].ptr = ptr;
    p->allocations[idx].size = size;

    return ptr;
}

static void process_allocation_unjoin(struct process *p, void *ptr)
{
    for (uint32_t i = 0; i < PEACHOS_MAX_PROGRAM_ALLOCATIONS; ++i) {
        if (p->allocations[i].ptr == ptr) {
            p->allocations[i].ptr = NULL;
            p->allocations[i].size = 0;
            break;
        }
    }
}

static struct process_allocation *get_allocation_by_addr(struct process *p,
                                                         void *addr)
{
    for (uint32_t i = 0; i < PEACHOS_MAX_PROGRAM_ALLOCATIONS; ++i) {
        if (p->allocations[i].ptr == addr)
            return &p->allocations[i];
    }

    return NULL;
}

int32_t process_count_command_arguments(struct cmdarg *root)
{
    int32_t i = 0;
    struct cmdarg *cur = root;

    while (cur) {
        ++i;
        cur = cur->next;
    }

    return i;
}

int32_t process_inject_arguments(struct process *p, struct cmdarg *root)
{
    struct cmdarg *cur = root;

    const int32_t argc = process_count_command_arguments(root);
    if (!argc)
        return -EIO;

    char **argv = process_malloc(p, sizeof(*argv) * argc);
    if (!argv)
        return -ENOMEM;

    int32_t i = 0;
    while (cur) {
        char *arg = process_malloc(p, sizeof(cur->arg));
        if (!arg)
            goto arg_malloc_error;

        strncpy(arg, cur->arg, sizeof(cur->arg));
        argv[i] = arg;

        cur = cur->next;
        ++i;
    }

    // assert(argc == i)

    p->args.argc = argc;
    p->args.argv = argv;
    return 0;

arg_malloc_error:
    for (int32_t j = 0; j < i; ++j)
        process_free(p, argv[j]);
    process_free(p, argv);
    return -ENOMEM;
}

void process_get_arguments(struct process *p, int32_t *argc, char ***argv)
{
    *argc = p->args.argc;
    *argv = p->args.argv;
}

void process_free(struct process *p, void *ptr)
{
    struct process_allocation *alloc = get_allocation_by_addr(p, ptr);
    if (!alloc)
        return;

    const int32_t ret =
        paging_map_to(p->task->page_directory, alloc->ptr, alloc->ptr,
                      paging_align_address(alloc->ptr + alloc->size), 0);
    if (ret < 0)
        return;

    process_allocation_unjoin(p, ptr);

    kfree(ptr);
}

int32_t process_load_switch(const char *filename, struct process **p)
{
    int32_t ret = process_load(filename, p);
    if (ret < 0)
        return ret;
    return process_switch(*p);
}

static int32_t process_load_binary(const char *filename, struct process *p)
{
    int32_t ret = 0;
    void *program_data_ptr = NULL;

    int32_t fd = fs_fopen(filename, "r");
    if (fd <= 0)
        return -EIO;

    struct file_stat sb;
    ret = fs_fstat(fd, &sb);
    if (ret != 0)
        goto err_out;

    program_data_ptr = kzalloc(sb.file_size);
    if (!program_data_ptr)
        goto err_out;

    if (fs_fread(program_data_ptr, sb.file_size, 1, fd) != 1) {
        ret = -EIO;
        goto err_out;
    }

    p->file_type = PROCESS_FILE_TYPE_BINARY;
    p->ptr = program_data_ptr;
    p->size = sb.file_size;

    fs_fclose(fd);
    return 0;

err_out:
    fs_fclose(fd);

    if (program_data_ptr)
        kfree(program_data_ptr);

    return ret;
}

static int32_t process_load_elf(const char *filename, struct process *p)
{
    struct elf_file *elf = NULL;

    int32_t ret = elf_load(filename, &elf);
    if (ret < 0)
        return ret;

    p->file_type = PROCESS_FILE_TYPE_ELF;
    p->elf = elf;

    return 0;
}

static int32_t process_load_data(const char *filename, struct process *p)
{
    int32_t ret = process_load_elf(filename, p);
    if (ret == -EINFORMAT)
        ret = process_load_binary(filename, p);
    return ret;
}

static int32_t process_map_binary(struct process *p)
{
    return paging_map_to(p->task->page_directory,
                         (void *)PEACHOS_PROGRAM_VIRTUAL_ADDRESS, p->ptr,
                         paging_align_address(p->ptr + p->size),
                         PTE_PRESENT | PTE_USER_SUPERVISOR | PTE_READ_WRITE);
}

static int32_t process_map_elf(struct process *p)
{
    struct elf_file *elf = p->elf;

    struct elf_header *header = elf_header(elf);
    struct elf32_phdr *phdrs = elf_pheader(header);

    for (uint32_t i = 0; i < header->e_phnum; ++i) {
        struct elf32_phdr *phdr = &phdrs[i];

        void *phdr_paddr = elf_phdr_phys_address(elf, phdr);

        uint16_t ptflags = PTE_PRESENT | PTE_USER_SUPERVISOR;
        if (phdr->p_flags & PF_W) {
            ptflags |= PTE_READ_WRITE;
        }

        const int32_t ret = paging_map_to(
            p->task->page_directory,
            paging_align_to_lower_page((void *)phdr->p_vaddr),
            paging_align_to_lower_page(phdr_paddr),
            paging_align_address(phdr_paddr + phdr->p_memsz), ptflags);

        if (ret < 0)
            return ret;
    }

    return 0;
}

static int32_t process_map_memory(struct process *p)
{
    int32_t ret = 0;

    switch (p->file_type) {
    case PROCESS_FILE_TYPE_ELF:
        ret = process_map_elf(p);
        break;
    case PROCESS_FILE_TYPE_BINARY:
        ret = process_map_binary(p);
        break;
    default:
        panic("process_map_memory invalid file type\n");
    }

    if (ret < 0)
        return ret;

    return paging_map_to(
        p->task->page_directory,
        (void *)PEACHOS_PROGRAM_VIRTUAL_STACK_ADDRESS_END, p->stack,
        paging_align_address(p->stack + PEACHOS_USER_PROGRAM_STACK_SIZE),
        PTE_PRESENT | PTE_USER_SUPERVISOR | PTE_READ_WRITE);
}

int32_t process_get_free_slot(void)
{
    for (uint32_t i = 0; i < PEACHOS_MAX_PROCESSES; ++i) {
        if (!processes[i])
            return i;
    }

    return -EISTKN;
}

int32_t process_load_for_slot(const char *filename,
                              struct process **process_out, uint32_t slot)
{
    int32_t ret = 0;

    struct task *task = NULL;
    void *program_stack_ptr = NULL;

    if (processes[slot]) {
        ret = -EISTKN;
        goto out;
    }

    struct process *p = kzalloc(sizeof(*p));
    if (!p) {
        ret = -ENOMEM;
        goto out;
    }

    process_init(p);

    ret = process_load_data(filename, p);
    if (ret < 0)
        goto out;

    program_stack_ptr = kzalloc(PEACHOS_USER_PROGRAM_STACK_SIZE);
    if (!program_stack_ptr) {
        ret = -ENOMEM;
        goto out;
    }

    strncpy(p->filename, filename, sizeof(p->filename));
    p->stack = program_stack_ptr;
    p->id = slot;

    task = task_new(p);
    if (ERROR_I(task) == 0) {
        ret = ERROR_I(task);
        goto out;
    }

    p->task = task;

    ret = process_map_memory(p);
    if (ret < 0)
        goto out;

    *process_out = p;
    processes[slot] = p;
out:
    if (ISERR(ret)) {
        if (p && p->task)
            task_free(p->task);
        // Free the process data
    }
    return ret;
}

int32_t process_load(const char *filename, struct process **process_out)
{
    const int32_t process_slot = process_get_free_slot();
    if (process_slot < 0)
        return -ENOMEM;

    return process_load_for_slot(filename, process_out, process_slot);
}

void *process_invoke_system_command(const struct interrupt_frame *frame)
{
    (void)frame;

    struct cmdarg *args = task_virtual_address_to_physical(
        task_get_current(), task_get_stack_item(task_get_current(), 0));
    if (!args || !strlen(args[0].arg))
        return (void *)-EINVARG;

    struct cmdarg *cmd = &args[0];
    const char *program_name = cmd->arg;

    char pathname[PEACHOS_MAX_PATH];
    strcpy(pathname, "0:/");
    strcpy(&pathname[3], program_name);

    struct process *p = NULL;
    int32_t ret = process_load_switch(pathname, &p);
    if (ret < 0)
        return (void *)ret;

    process_inject_arguments(p, cmd);

    task_switch(p->task);
    task_return(&p->task->registers);

    return NULL;
}

static void process_terminate_allocations(struct process *p)
{
    for (uint32_t i = 0; i < PEACHOS_MAX_PROGRAM_ALLOCATIONS; ++i) {
        process_free(p, p->allocations[i].ptr);
    }
}

static void process_free_program_data(struct process *p)
{
    switch (p->file_type) {
    case PROCESS_FILE_TYPE_ELF:
        elf_close(p->elf);
        break;
    case PROCESS_FILE_TYPE_BINARY:
        kfree(p->ptr);
        break;
    }
}

static void process_switch_to_any(void)
{
    for (uint32_t i = 0; i < PEACHOS_MAX_PROCESSES; ++i) {
        if (processes[i]) {
            process_switch(processes[i]);
            return;
        }
    }

    panic("No processes to switch too\n");
}

static void process_unlink(struct process *p)
{
    processes[p->id] = NULL;
    if (current_process == p)
        process_switch_to_any();
}

void process_terminate(struct process *p)
{
    process_terminate_allocations(p);
    process_free_program_data(p);

    kfree(p->stack);
    task_free(p->task);

    process_unlink(p);
}
