#include "task.h"
#include "elf_loader.h"
#include "idt.h"
#include "kheap.h"
#include "paging.h"
#include "string.h"
#include "status.h"
#include "kernel.h"
#include "process.h"

#define USER_DATA_SEGMENT 0x23
#define USER_CODE_SEGMENT 0x1b

static struct task *current_task;

static struct task *task_head;
static struct task *task_tail;

struct task *task_get_current(void)
{
    return current_task;
}

struct task *task_get_next(void)
{
    if (!current_task->task_next)
        return task_head;

    return current_task->task_next;
}

void task_next(void)
{
    struct task *next_task = task_get_next();
    if (!next_task)
        panic("No more tasks!\n");
    task_switch(next_task);
    task_return(&next_task->registers);
}

static void task_list_remove(struct task *task)
{
    if (task->task_prev)
        task->task_prev->task_next = task->task_next;

    if (task == task_head)
        task_head = task->task_next;

    if (task == task_tail)
        task_tail = task->task_prev;

    if (task == current_task)
        current_task = task_get_next();
}

void task_free(struct task *task)
{
    paging_free_4gb(task->page_directory);
    task_list_remove(task);
    kfree(task);
}

static int32_t task_init(struct task *task, struct process *p)
{
    memset(task, 0, sizeof(*task));

    task->page_directory = paging_new_4gb(PTE_PRESENT | PTE_USER_SUPERVISOR,
                                          PTD_PRESENT | PTD_USER_SUPERVISOR);
    if (!task->page_directory) {
        return -EIO;
    }

    if (p->file_type == PROCESS_FILE_TYPE_ELF) {
        task->registers.ip = elf_header(p->elf)->e_entry;
    } else {
        task->registers.ip = PEACHOS_PROGRAM_VIRTUAL_ADDRESS;
    }

    task->registers.ss = USER_DATA_SEGMENT;
    task->registers.cs = USER_CODE_SEGMENT;
    task->registers.esp = PEACHOS_PROGRAM_VIRTUAL_STACK_ADDRESS_START;

    task->process = p;

    return 0;
}

struct task *task_new(struct process *p)
{
    int32_t ret = 0;

    struct task *task = kzalloc(sizeof(*task));
    if (!task) {
        ret = -ENOMEM;
        goto out;
    }

    ret = task_init(task, p);
    if (ret != 0)
        goto out;

    if (!task_head) {
        task_head = task;
        task_tail = task;
        current_task = task;
        goto out;
    }

    task_tail->task_next = task;
    task->task_prev = task_tail;
    task_tail = task;

out:
    if (ISERR(ret)) {
        task_free(task);
        return ERROR(ret);
    }
    return task;
}

void task_run_first_ever_task(void)
{
    if (!current_task) {
        panic("task_run_first_ever_task(): No current task exists!\n");
    }

    task_switch(task_head);
    task_return(&task_head->registers);
}

int32_t task_page(void)
{
    user_registers();
    task_switch(current_task);
    return 0;
}

int32_t task_page_task(const struct task *task)
{
    user_registers();
    paging_switch(task->page_directory);
    return 0;
}

int32_t task_switch(struct task *task)
{
    current_task = task;
    paging_switch(task->page_directory);
    return 0;
}

static void task_save_state(struct task *task,
                            const struct interrupt_frame *frame)
{
    task->registers.ip = frame->ip;
    task->registers.cs = frame->cs;
    task->registers.flags = frame->flags;
    task->registers.esp = frame->esp;
    task->registers.ss = frame->ss;
    task->registers.eax = frame->eax;
    task->registers.ebp = frame->ebp;
    task->registers.ebx = frame->ebx;
    task->registers.ecx = frame->ecx;
    task->registers.edi = frame->edi;
    task->registers.edx = frame->edx;
    task->registers.esi = frame->esi;
}

void task_current_save_state(const struct interrupt_frame *frame)
{
    struct task *current = task_get_current();
    if (!current) {
        panic("No current task to save\n");
    }

    task_save_state(current, frame);
}

int32_t copy_str_from_task(struct task *task, void *virt, void *phys,
                           uint32_t max)
{
    if (max >= PAGING_PAGE_SIZE)
        return -EINVARG;

    int32_t ret = 0;

    char *tmp = kzalloc(max);
    if (!tmp) {
        ret = -ENOMEM;
        goto out;
    }

    // NOTE(José): Get the old physical address and flags for the `tmp` virtual address.
    void *old_addr = 0;
    uint16_t old_flags = 0;
    paging_get(task->page_directory, tmp, &old_addr, &old_flags);

    // NOTE(José): Map the `tmp` vaddr to `tmp` addr.
    paging_map(task->page_directory, tmp, tmp,
               PTE_PRESENT | PTE_USER_SUPERVISOR | PTE_READ_WRITE);

    // NOTE(José): Switch pages now that we share the `tmp` memory with the task, copy the string and return to kernel pages.
    paging_switch(task->page_directory);
    strncpy(tmp, virt, max);
    kernel_page();

    // NOTE(José): Restore the original page entry of the task.
    ret = paging_map(task->page_directory, tmp, old_addr, old_flags);
    if (ret < 0) {
        ret = -EIO;
        goto out_free;
    }

    strncpy(phys, tmp, max);

out_free:
    kfree(tmp);

out:
    return ret;
}

void *task_get_stack_item(struct task *task, uint32_t idx)
{
    // NOTE(José): We get the pointer before changing the pages because
    // task is a pointer in kernel space!
    uint32_t *sp_ptr = (uint32_t *)task->registers.esp;
    task_page_task(task);

    // NOTE(José): Now get the real pointer...
    void *result = (void *)sp_ptr[idx];

    kernel_page();
    return result;
}

void *task_virtual_address_to_physical(struct task *task, void *vaddr)
{
    return paging_get_physical_addr(task->page_directory, vaddr);
}
