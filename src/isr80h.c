#include "isr80h.h"
#include "idt.h"
#include "kernel.h"
#include "keyboard.h"
#include "path.h"
#include "task.h"
#include "process.h"
#include "string.h"

#include <stddef.h>

static void *isr80h_command0_sum(const struct interrupt_frame *frame)
{
    (void)frame;
    const int32_t v2 = (int32_t)task_get_stack_item(task_get_current(), 1);
    const int32_t v1 = (int32_t)task_get_stack_item(task_get_current(), 0);
    return (void *)(v1 + v2);
}

static void *isr80h_command1_print(const struct interrupt_frame *frame)
{
    (void)frame;

    char buffer[1024];

    void *user_space_msg_buffer = task_get_stack_item(task_get_current(), 0);
    copy_str_from_task(task_get_current(), user_space_msg_buffer, buffer,
                       sizeof(buffer));

    print(buffer);
    return 0;
}

static void *isr80h_command2_get_key(const struct interrupt_frame *frame)
{
    (void)frame;
    const int c = keyboard_pop();
    return (void *)c;
}

static void *isr80h_command3_putc(const struct interrupt_frame *frame)
{
    (void)frame;
    const int32_t c = (int32_t)task_get_stack_item(task_get_current(), 0);
    putc((char)c);
    return 0;
}

static void *isr80h_command4_malloc(const struct interrupt_frame *frame)
{
    (void)frame;
    const size_t size = (int32_t)task_get_stack_item(task_get_current(), 0);
    return process_malloc(task_get_current()->process, size);
}

static void *isr80h_command5_free(const struct interrupt_frame *frame)
{
    (void)frame;
    void *ptr = (void *)task_get_stack_item(task_get_current(), 0);
    process_free(task_get_current()->process, ptr);
    return 0;
}

static void *isr80h_command6_create_process(const struct interrupt_frame *frame)
{
    (void)frame;
    void *filename_user_ptr = (void *)task_get_stack_item(task_get_current(), 0);

    char filename[PEACHOS_MAX_PATH];
    int32_t ret = copy_str_from_task(task_get_current(), filename_user_ptr, filename, sizeof(filename));
    if (ret < 0)
        return (void *)ret;

    char pathname[PEACHOS_MAX_PATH];
    strcpy(pathname, "0:/");
    strcpy(&pathname[3], filename);

    struct process *p = NULL;
    ret = process_load_switch(pathname, &p);
    if (ret < 0)
        return (void *)ret;

    task_switch(p->task);
    task_return(&p->task->registers);

    return 0;
}

static void *isr80h_command7_invoke_system_command(const struct interrupt_frame *frame)
{
    return process_invoke_system_command(frame);
}

static void *isr80h_command8_get_program_arguments(const struct interrupt_frame *frame)
{
    (void)frame;
    struct process *p = task_get_current()->process;
    struct procarg *args = task_virtual_address_to_physical(task_get_current(), task_get_stack_item(task_get_current(), 0));
    process_get_arguments(p, &args->argc, &args->argv);
    return 0;
}

static void *isr80h_command9_exit(const struct interrupt_frame *frame)
{
    (void)frame;
    process_terminate(task_get_current()->process);
    task_next();
    return 0;
}

void isr80h_register_commands(void)
{
    isr80h_register_command(SYS_COMMAND_SUM, isr80h_command0_sum);
    isr80h_register_command(SYS_COMMAND_PRINT, isr80h_command1_print);
    isr80h_register_command(SYS_COMMAND_GET_KEY, isr80h_command2_get_key);
    isr80h_register_command(SYS_COMMAND_PUTC, isr80h_command3_putc);
    isr80h_register_command(SYS_COMMAND_MALLOC, isr80h_command4_malloc);
    isr80h_register_command(SYS_COMMAND_FREE, isr80h_command5_free);
    isr80h_register_command(SYS_COMMAND_CREATE_PROCESS, isr80h_command6_create_process);
    isr80h_register_command(SYS_COMMAND_INVOKE_SYSTEM_COMMAND, isr80h_command7_invoke_system_command);
    isr80h_register_command(SYS_COMMAND_GET_PROGRAM_ARGUMENTS, isr80h_command8_get_program_arguments);
    isr80h_register_command(SYS_COMMAND_EXIT, isr80h_command9_exit);
}
