#include "disk_stream.h"
#include "config.h"
#include "disk.h"
#include "kheap.h"

int32_t disk_stream_init(uint32_t disk_id, struct disk_stream *stream)
{
    struct disk *disk = disk_get(disk_id);
    if (!disk)
        return -1;

    stream->pos = 0;
    stream->disk = disk;
    return 0;
}

void disk_stream_seek(struct disk_stream *stream, uint64_t pos)
{
    stream->pos = pos;
}

int32_t disk_stream_read(struct disk_stream *stream, uint32_t length,
                         uint8_t *buffer)
{
    // TODO(José): Although we ask the disk to read one block, we are only allocating enough for
    // a single sector. Yes, we know that in this case the block size is equal to the sector size,
    // but what if it wasn't? We might need to ask the disk what it's block size is.
    uint8_t block_buffer[PEACHOS_DISK_SECTOR_SIZE];
    int32_t ret = 0;

    while (length != 0) {
        const uint64_t sector = stream->pos >> PEACHOS_DISK_SECTOR_SIZE_BITS;
        const uint64_t offset = stream->pos & (PEACHOS_DISK_SECTOR_SIZE - 1);

        ret = disk_read_block(stream->disk, sector, 1, block_buffer);
        if (ret < 0)
            goto out;

        const uint32_t available_now = PEACHOS_DISK_SECTOR_SIZE - offset;
        const uint32_t count = length > available_now ? available_now : length;
        for (uint32_t i = 0; i < count; ++i)
            buffer[i] = block_buffer[offset + i];

        stream->pos += count;
        buffer += count;
        length -= count;
    }

out:
    return ret;
}
