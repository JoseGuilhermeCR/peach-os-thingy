org 0x7c00
bits 16

CODE_SEG equ gdt_code - gdt_start
DATA_SEG equ gdt_data - gdt_start

; NOTE(José): https://wiki.osdev.org/FAT#BPB_.28BIOS_Parameter_Block.29
; Reserve enough memory for the BPB (BIOS Parameter Block) and jump to our actual code.
jmp short start
nop

; FAT16 Header
OEMIdentifier     db 'PEACHOS '
BytesPerSector    dw 0x200
SectorsPerCluster db 0x80
; TODO(José): Place our kernel in a file instead of in these reserved sectors.
ReservedSectors   dw 200
FATCopies         db 0x02
RootDirEntries    dw 0x40
NumSectors        dw 0x00
MediaType         db 0xf8
SectorsPerFat     dw 0x100
SectorsPerTrack   dw 0x20
NumberOfHeads     dw 0x40
HiddenSectors     dd 0x00
SectorsBig        dd 0x773594

; Extended BPB (DOS 4.0)
DriveNumber       db 0x80
WinNTBit          db 0x00
Signature         db 0x29
VolumeID          dd 0xd105
VolumeIDString    db 'PEACHOS BOO'
SystemIDString    db 'FAT16   '

start:
    ; NOTE(José): This is a jump in order to change the code segment register.
    jmp 0:step2

step2:
    ; NOTE(José): https://www.felixcloutier.com/x86/cli
    ; Clear interrupt flags and disable interrupts.
    ; The idea is that we don't want an interrupt to happen while we haven't finished setting up the segment registers.
    cli
    xor ax, ax
    mov ds, ax
    mov es, ax
    mov ss, ax
    mov sp, 0x7c00
    ; NOTE(José): https://www.felixcloutier.com/x86/sti
    ; Enable interrupts.
    sti

.load_protected:
    cli
    lgdt[gdt_descriptor]
    mov eax, cr0
    or eax, 0x1
    mov cr0, eax
    jmp CODE_SEG:load32

; GDT:
; NOTE(José): https://wiki.osdev.org/Global_Descriptor_Table
gdt_start:
; Offset 0x0
gdt_null:
    dd 0
    dd 0
; Offset 0x8 CS
gdt_code:
    ; NOTE(José): Limit [15:0]
    dw 0xffff
    ; NOTE(José): Base [15:0]
    dw 0x0000
    ; NOTE(José): Base [23:16]
    db 0x00
    ; NOTE(José): Access Byte
    ; P Bit | S Bit | E Bit | RW Bit
    db 0x9a
    ; NOTE(José): Flags (Upper Nibble) and (Lower Nibble) Limit [20:16]
    ; Flags: G | DB
    db 0xCF   ; Flags
    db 0      ; Base 24-31 bits
; Offset 0x10 DS, SS, ES, FS, GS
gdt_data:
    ; NOTE(José): Limit [15:0]
    dw 0xffff
    ; NOTE(José): Base [15:0]
    dw 0x0000
    ; NOTE(José): Base [23:16]
    db 0x00
    ; NOTE(José): Access Byte
    ; P Bit | S Bit | RW Bit
    db 0x92
    ; NOTE(José): Flags (Upper Nibble) and (Lower Nibble) Limit [20:16]
    ; Flags: G | DB
    db 0xCF   ; Flags
    db 0      ; Base 24-31 bits
gdt_end:
gdt_descriptor:
    dw gdt_end - gdt_start - 1
    dd gdt_start

bits 32
load32:
    mov eax, 1
    mov ecx, 100
    mov edi, 0x100000
    call ata_lba_read
    jmp CODE_SEG:0x100000

ata_lba_read:
    mov ebx, eax ; Backup the LBA
    ; NOTE(José): Dear instructor does not explain it...:
    ; https://wiki.osdev.org/ATA_PIO_Mode

    ; Send the highest 8 bits of the LBA to the hard disk controller
    ; NOTE(José): 6 - Drive / Head Register
    shr eax, 24
    ; Select the master drive
    or eax, 0xE0
    mov dx, 0x1f6
    out dx, al

    ; Send the total of sectors to read
    ; NOTE(José): 2 - Sector Count Register
    mov eax, ecx
    mov dx, 0x1f2
    out dx, al

    ; Send more bits of the LBA
    ; NOTE(José): 3 - Sector Number Register / (LBAlo)
    mov eax, ebx
    mov dx, 0x1f3
    out dx, al

    ; Send more bits of the LBA
    ; NOTE(José): 4 - Cylinder High Register / (LBAmid)
    mov eax, ebx
    shr eax, 8
    mov dx, 0x1f4
    out dx, al

    ; Send upper 16-bits of the LBA
    ; NOTE(José): 5 - Cylinder High Register / (LBAhi)
    mov eax, ebx
    shr eax, 16
    mov dx, 0x1f5
    out dx, al

    ; NOTE(José): 7 - Command Register
    mov al, 0x20
    mov dx, 0x1f7
    out dx, al

    ; Read all sectors into memory
.next_sector:
    push ecx

.try_again:
    ; NOTE(José): 7 - Status Register
    mov dx, 0x1f7
    in al, dx
    test al, 8
    jz .try_again

    ; We need to read 256 words at a time
    mov ecx, 256
    mov dx, 0x1f0
    rep insw
    pop ecx
    loop .next_sector
    ; End of reading sesctors into memory
    ret

times 510 - ($ - $$) db 0
dw 0xaa55
