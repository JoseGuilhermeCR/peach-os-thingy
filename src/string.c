#include "string.h"

char tolower(char c)
{
    if (c >= 'A' && c <= 'Z')
        c += 32;
    return c;
}

size_t strlen(const char *str)
{
    size_t len = 0;
    while (str[len] != '\0')
        ++len;
    return len;
}

int32_t istrncmp(const char *s1, const char *s2, int32_t n)
{
    uint8_t u1;
    uint8_t u2;

    while (n-- > 0) {
        u1 = (uint8_t)*s1++;
        u2 = (uint8_t)*s2++;

        if (u1 != u2 && tolower(u1) != tolower(u2))
            return u1 - u2;

        if (u1 == '\0')
            return 0;
    }

    return 0;
}

size_t strnlen_terminator(const char *str, size_t size, char terminator)
{
    for (size_t i = 0; i < size; ++i)
        if (str[i] == '\0' || str[i] == terminator)
            return i;

    return 0;
}

void *memcpy(void *dest, void *src, size_t size)
{
    uint8_t *d = dest;
    uint8_t *s = src;

    while (size--) {
        *d = *s;
        ++d;
        ++s;
    }

    return dest;
}

int32_t memcmp(const void *s1, const void *s2, size_t size)
{
    const char *c1 = s1;
    const char *c2 = s2;

    while (size-- > 0) {
        if (*c1++ != *c2++)
            return c1[-1] < c2[-1] ? -1 : 1;
    }

    return 0;
}

void *memset(void *ptr, int c, size_t size)
{
    char *c_ptr = (char *)ptr;
    for (size_t i = 0; i < size; ++i)
        c_ptr[i] = (char)c;
    return ptr;
}

char *strncpy(char *dest, const char *src, size_t size)
{
    size_t i = 0;

    for (; i < size; ++i) {
        if (!src[i])
            break;
        dest[i] = src[i];
    }

    dest[i] = 0;
    return dest;
}

char *strcpy(char *dest, const char *src)
{
    char *res = dest;

    while (*src) {
        *dest = *src;
        ++dest;
        ++src;
    }

    *dest = '\0';
    return res;
}
