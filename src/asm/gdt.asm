section .asm exec

global gdt_load

; NOTE(José): I didn't adjust the frame pointer because this guy didn't and
; I don't care about it anymore really.
gdt_load:
    ; push ebp
    ; mov ebp, esp

    mov eax, [esp + 4]
    mov [gdt_descriptor + 2], eax
    mov ax, [esp + 8]
    mov [gdt_descriptor], ax
    lgdt [gdt_descriptor]

    ; pop ebp
    ret

section .data
gdt_descriptor:
    dw 0x00
    dd 0x00

