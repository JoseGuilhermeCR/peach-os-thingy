bits 32

; NOTE(José): CODE and DATA segments as defined in boot.asm.
CODE_SEG equ 0x08
DATA_SEG equ 0x10

extern kernel_init

global _start
global kernel_registers

; NOTE(José): This section is placed in front of all others so that
; this label ends up at the address expected by boot.asm.
section .kernel_start exec
_start:
    mov ax, DATA_SEG
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax
    mov ss, ax

    mov ebp, 0x200000
    mov esp, ebp

    ; Enable the A20 line
    in al, 0x92
    or al, 2
    out 0x92, al

    ; Remap the master PIC
    mov al, 10001b
    out 0x20, al ; Tell master PIC

    mov al, 0x20 ; Interrupt 0x20 is where master ISR should start
    out 0x21, al

    mov al, 1;
    out 0x21, al

    call kernel_init
    jmp $

; NOTE(José): Don't think aligning this is necessary since the sections
; themselves are aligned in the linker script.
; times 512 - ($ - $$) db 0

section .asm exec
kernel_registers:
    mov ax, 0x10
    mov dx, ax
    mov es, ax
    mov gs, ax
    mov fs, ax
    ret

