// NOTE(José): Overall awful code quality here, but I need to finish
// this ASAP. At this point I've stopped doing changes to the instructor
// code and god how awful it is..

#include "fat16.h"

#include <stddef.h>
#include <stdbool.h>

#include "path.h"
#include "kernel.h"
#include "disk_stream.h"
#include "kheap.h"
#include "file.h"
#include "status.h"
#include "string.h"

#define SIGNATURE 0x29
#define FAT16_FAT_ENTRY_SIZE 0x02
#define FAT16_BAD_SECTOR 0xff7
#define FA16_UNUSED 0x00

#define FAT_FILE_READ_ONLY 0x01
#define FAT_FILE_HIDDEN 0x02
#define FAT_FILE_SYSTEM 0x04
#define FAT_FILE_VOLUME_LABEL 0x08
#define FAT_FILE_SUBDIRECTORY 0x10
#define FAT_FILE_ARCHIVE 0x20
#define FAT_FILE_DEVICE 0x40
#define FAT_FILE_RESERVED 0x80

enum fat_item_type {
    FAT_ITEM_TYPE_DIR = 0,
    FAT_ITEM_TYPE_FILE = 1,
};

struct fat_extended_header {
    uint8_t drive_number;
    uint8_t win_nt_bit;
    uint8_t signature;
    uint32_t volume_id;
    uint8_t volume_id_string[11];
    uint8_t system_id_string[8];
} __attribute__((packed));

struct fat_primary_header {
    uint8_t short_jmp_ins[3];
    uint8_t oem_identifier[8];
    uint16_t bytes_per_sector;
    uint8_t sectors_per_cluster;
    uint16_t reserved_sectors;
    uint8_t fat_copies;
    uint16_t root_directory_entries;
    uint16_t number_of_sectors;
    uint8_t media_type;
    uint16_t sectors_per_fat;
    uint16_t sectors_per_track;
    uint16_t number_of_heads;
    uint32_t hidden_sectors;
    uint32_t sectors_big;
} __attribute__((packed));

struct fat_header {
    struct fat_primary_header primary_header;
    union {
        struct fat_extended_header extended_header;
    } shared;
};

struct fat_directory_item {
    uint8_t filename[8];
    uint8_t ext[3];
    uint8_t attribute;
    uint8_t reserved;
    uint8_t creation_time_tenths_of_a_sec;
    uint16_t creation_time;
    uint16_t creation_date;
    uint16_t last_access;
    uint16_t high_16_bits_first_cluster;
    uint16_t last_mode_time;
    uint16_t last_mode_date;
    uint16_t low_16_bits_first_cluster;
    uint32_t file_size;
} __attribute__((packed));

struct fat_directory {
    struct fat_directory_item *items;
    uint32_t items_length;
    uint64_t sector_pos;
    uint64_t ending_sector_pos;
};

struct fat_item {
    union {
        struct fat_directory_item *item;
        struct fat_directory *directory;
    };
    enum fat_item_type type;
};

struct fat_file_descriptor {
    struct fat_item *item;
    uint32_t pos;
};

struct fat_private_data {
    struct fat_header header;
    struct fat_directory root_directory;

    // NOTE(José): The other streams are actually pointers from
    // inside the streams array so that we can perform a single allocation.
    // Only this pointer should be freed.
    struct disk_stream *streams;

    struct disk_stream *cluster_read_stream;
    struct disk_stream *fat_read_stream;
    struct disk_stream *directory_stream;
};

static void *fat16_open(struct disk *disk, struct path_part *path,
                        uint32_t mode);
static int32_t fat16_resolve(struct disk *disk);
static int32_t fat16_seek(void *descriptor, uint32_t offset,
                          enum file_seek_mode whence);
static int32_t fat16_read(struct disk *disk, void *descriptor, uint32_t size,
                          uint32_t nmemb, char *out_ptr);
static int32_t fat16_fstat(void *descriptor, struct file_stat *sb);
static int32_t fat16_close(void *descriptor);

static struct fs fat16_fs = {
    .open = fat16_open,
    .resolve = fat16_resolve,
    .seek = fat16_seek,
    .read = fat16_read,
    .fstat = fat16_fstat,
    .close = fat16_close,
    .name = "FAT16",
};

static void fat16_to_proper_string(const char *in, char **out)
{
    while (*in && *in != ' ') {
        **out = *in;
        ++*out;
        ++in;
    }

    if (*in == ' ')
        **out = '\0';
}

static void fat16_get_full_relative_filename(struct fat_directory_item *item,
                                             uint32_t length, char *out)
{
    memset(out, 0, length);

    char *tmp_out = out;

    fat16_to_proper_string((const char *)item->filename, &tmp_out);
    if (item->ext[0] && item->ext[0] != ' ') {
        *tmp_out++ = '.';
        fat16_to_proper_string((const char *)item->ext, &tmp_out);
    }
}

static struct fat_directory_item *
fat16_clone_directory_item(struct fat_directory_item *item)
{
    struct fat_directory_item *item_copy = kmalloc(sizeof(*item));
    if (!item_copy)
        return NULL;

    memcpy(item_copy, item, sizeof(*item));
    return item_copy;
}

static uint32_t fat16_get_first_cluster(struct fat_directory_item *item)
{
    // FIXME(José): he doesn't shift the high 16 bits?!
    return (item->high_16_bits_first_cluster << 16) |
           item->low_16_bits_first_cluster;
}

static uint32_t fat16_cluster_to_sector(struct fat_private_data *private,
                                        uint32_t cluster)
{
    if (cluster < 2) { /* TODO(José): PANIC */
    }
    return private->root_directory.ending_sector_pos +
           ((cluster - 2) * private->header.primary_header.sectors_per_cluster);
}

static uint32_t fat16_get_first_fat_sector(struct fat_private_data *private)
{
    return private->header.primary_header.reserved_sectors;
}

static int32_t fat16_get_fat_entry(struct disk *disk, int32_t cluster)
{
    struct fat_private_data *private = disk->fs_private_data;
    struct disk_stream *stream = private->fat_read_stream;

    if (!stream)
        return -1;

    const uint32_t fat_table_pos =
        fat16_get_first_fat_sector(private) * disk->sector_size;
    disk_stream_seek(stream, fat_table_pos * (cluster * FAT16_FAT_ENTRY_SIZE));

    uint16_t entry = 0;
    if (disk_stream_read(stream, sizeof(entry), (uint8_t *)&entry) != 0)
        return -1;

    return entry;
}

static int32_t fat16_get_cluster_for_offset(struct disk *disk,
                                            int32_t start_cluster,
                                            int32_t offset)
{
    struct fat_private_data *private = disk->fs_private_data;

    const int32_t size_of_cluster_bytes =
        private->header.primary_header.sectors_per_cluster * disk->sector_size;
    const int32_t clusters_ahead = offset / size_of_cluster_bytes;

    for (int32_t i = 0; i < clusters_ahead; ++i) {
        const int32_t entry = fat16_get_fat_entry(disk, start_cluster);
        if (entry == 0xff8 || entry == 0xfff)
            return -EIO;

        if (entry == FAT16_BAD_SECTOR)
            return -EIO;

        if (entry == 0xff0 || entry == 0xff6)
            return -EIO;

        if (entry == 0x00)
            return -EIO;

        return entry;
    }

    return start_cluster;
}

// NOTE(José): Note how he is consistent with the naming below! fat16_free_* vs fat16_*_free! :^D
static void fat16_free_directory(struct fat_directory *dir)
{
    if (!dir)
        return;

    if (dir->items)
        kfree(dir->items);

    kfree(dir);
}

static void fat16_fat_item_free(struct fat_item *item)
{
    if (item->type == FAT_ITEM_TYPE_DIR) {
        fat16_free_directory(item->directory);
    } else if (item->type == FAT_ITEM_TYPE_FILE) {
        kfree(item->item);
    }

    kfree(item);
}

static int32_t fat16_read_internal_from_stream(struct disk *disk,
                                               struct disk_stream *stream,
                                               int32_t cluster, int32_t offset,
                                               int32_t total, void *out)
{
    struct fat_private_data *private = disk->fs_private_data;
    const int32_t size_of_cluster_bytes =
        private->header.primary_header.sectors_per_cluster * disk->sector_size;
    const int32_t cluster_to_use =
        fat16_get_cluster_for_offset(disk, cluster, offset);
    if (cluster_to_use < 0)
        return cluster_to_use;

    const int32_t offset_from_cluster = offset % size_of_cluster_bytes;

    const int32_t start_sector =
        fat16_cluster_to_sector(private, cluster_to_use);
    const int32_t start_pos =
        start_sector * disk->sector_size + offset_from_cluster;

    const int32_t total_to_read =
        total > size_of_cluster_bytes ? size_of_cluster_bytes : total;

    disk_stream_seek(stream, start_pos);
    if (disk_stream_read(stream, total_to_read, out) < 0)
        return -1;

    total -= total_to_read;
    if (total > 0) {
        // NOTE(José): This guy sure loves recursion huh.
        return fat16_read_internal_from_stream(disk, stream, cluster,
                                               offset + total_to_read, total,
                                               out + total_to_read);
    }

    return cluster_to_use;
}

static int32_t fat16_read_internal(struct disk *disk, int32_t start_cluster,
                                   int32_t offset, int32_t total, void *out)
{
    struct fat_private_data *private = disk->fs_private_data;
    struct disk_stream *stream = private->cluster_read_stream;
    return fat16_read_internal_from_stream(disk, stream, start_cluster, offset,
                                           total, out);
}

static int32_t fat16_get_total_items_for_directory(struct disk *disk,
                                                   uint32_t start_sector)
{
    int32_t ret = 0;
    int32_t total = 0;

    struct fat_directory_item item;
    struct fat_directory_item empty_item;

    memset(&empty_item, 0, sizeof(empty_item));

    struct fat_private_data *data = disk->fs_private_data;
    const uint32_t start_pos = start_sector * disk->sector_size;

    disk_stream_seek(data->directory_stream, start_pos);
    while (true) {
        ret = disk_stream_read(data->directory_stream, sizeof(item),
                               (uint8_t *)&item);
        if (ret < 0)
            return ret;

        if (item.filename[0] == 0x00)
            break;

        if (item.filename[0] == 0xe5)
            continue;

        ++total;
    }

    return total;
}

static struct fat_directory *
fat16_load_fat_directory(struct disk *disk, struct fat_directory_item *item)
{
    struct fat_private_data *fat_private = disk->fs_private_data;

    if (!(item->attribute & FAT_FILE_SUBDIRECTORY))
        return NULL;

    struct fat_directory *dir = kzalloc(sizeof(*dir));
    if (!dir)
        return NULL;

    const int32_t cluster = fat16_get_first_cluster(item);
    const int32_t cluster_sector =
        fat16_cluster_to_sector(fat_private, cluster);
    const int32_t total_items =
        fat16_get_total_items_for_directory(disk, cluster_sector);
    dir->items_length = total_items;

    const int32_t dir_size =
        dir->items_length * sizeof(struct fat_directory_item);
    dir->items = kzalloc(dir_size);
    if (!dir->items) {
        fat16_free_directory(dir);
        return NULL;
    }

    int32_t ret =
        fat16_read_internal(disk, cluster, 0x00, dir_size, dir->items);
    if (ret != 0) {
        fat16_free_directory(dir);
        return NULL;
    }

    return dir;
}

static struct fat_item *
fat16_new_fat_item_for_directory_item(struct disk *disk,
                                      struct fat_directory_item *item)
{
    struct fat_item *f_item = kzalloc(sizeof(*f_item));
    if (!f_item)
        return NULL;

    if (item->attribute & FAT_FILE_SUBDIRECTORY) {
        f_item->directory = fat16_load_fat_directory(disk, item);
        f_item->type = FAT_ITEM_TYPE_DIR;
        return f_item;
    }

    f_item->type = FAT_ITEM_TYPE_FILE;
    f_item->item = fat16_clone_directory_item(item);
    return f_item;
}

static struct fat_item *
fat16_find_item_in_directory(struct disk *disk, struct fat_directory *directory,
                             const char *name)
{
    struct fat_item *f_item = NULL;
    char tmp_filename[PEACHOS_MAX_PATH];

    for (uint32_t i = 0; i < directory->items_length; ++i) {
        fat16_get_full_relative_filename(&directory->items[i],
                                         sizeof(tmp_filename), tmp_filename);
        if (istrncmp(tmp_filename, name, sizeof(tmp_filename)) == 0) {
            // Found it let's create a new fat_item.
            f_item = fat16_new_fat_item_for_directory_item(
                disk, &directory->items[i]);
        }
    }

    return f_item;
}

static struct fat_item *fat16_get_directory_entry(struct disk *disk,
                                                  struct path_part *path)
{
    struct fat_private_data *private_data = disk->fs_private_data;
    struct fat_item *current_item = NULL;
    struct fat_item *root_item = fat16_find_item_in_directory(
        disk, &private_data->root_directory, path->str);

    if (!root_item)
        goto out;

    struct path_part *next_part = path->next;

    current_item = root_item;
    while (next_part) {
        if (current_item->type != FAT_ITEM_TYPE_DIR) {
            current_item = NULL;
        }

        struct fat_item *tmp_item = fat16_find_item_in_directory(
            disk, current_item->directory, next_part->str);
        fat16_fat_item_free(current_item);
        current_item = tmp_item;
        next_part = next_part->next;
    }

out:
    return current_item;
}

static void *fat16_open(struct disk *disk, struct path_part *path,
                        uint32_t mode)
{
    if (mode != FILE_MODE_BIT_READ) {
        return ERROR(-ERDONLY);
    }

    struct fat_file_descriptor *desc = kzalloc(sizeof(*desc));
    if (!desc)
        return ERROR(-ENOMEM);

    desc->item = fat16_get_directory_entry(disk, path);
    if (!desc->item)
        return ERROR(-EIO);

    desc->pos = 0;
    return desc;
}

static int32_t fat16_init_private_data(struct fat_private_data *data,
                                       struct disk *disk)
{
    memset(data, 0, sizeof(*data));

    struct disk_stream *streams = kmalloc(sizeof(*streams) * 3);
    if (!streams)
        return -ENOMEM;

    data->streams = streams;

    data->cluster_read_stream = &streams[0];
    data->fat_read_stream = &streams[1];
    data->directory_stream = &streams[2];

    int32_t ret = disk_stream_init(disk->id, data->cluster_read_stream);
    if (ret < 0)
        goto error_out;

    ret = disk_stream_init(disk->id, data->fat_read_stream);
    if (ret < 0)
        goto error_out;

    ret = disk_stream_init(disk->id, data->directory_stream);
    if (ret < 0)
        goto error_out;

    return 0;

error_out:
    kfree(data->streams);
    return ret;
}

static uint32_t fat16_sector_to_absolute(struct disk *disk, uint32_t sector)
{
    return sector * disk->sector_size;
}

static int32_t fat16_get_root_directory(struct disk *disk,
                                        struct fat_private_data *data,
                                        struct fat_directory *directory)
{
    int32_t ret = 0;

    struct fat_primary_header *primary_header = &data->header.primary_header;

    const uint32_t root_dir_sector_pos =
        primary_header->fat_copies * primary_header->sectors_per_fat +
        primary_header->reserved_sectors;
    const uint32_t root_dir_entries = primary_header->root_directory_entries;
    const uint32_t root_dir_size =
        root_dir_entries * sizeof(struct fat_directory_item);

    uint32_t total_sectors = root_dir_size / disk->sector_size;
    if (root_dir_size % disk->sector_size)
        ++total_sectors;

    uint32_t total_items =
        fat16_get_total_items_for_directory(disk, root_dir_sector_pos);

    struct fat_directory_item *dir = kzalloc(root_dir_size);
    if (!dir) {
        ret = -ENOMEM;
        goto error_out;
    }

    disk_stream_seek(data->directory_stream,
                     fat16_sector_to_absolute(disk, root_dir_sector_pos));

    ret =
        disk_stream_read(data->directory_stream, root_dir_size, (uint8_t *)dir);
    if (ret < 0)
        goto read_error_out;

    directory->items = dir;
    directory->items_length = total_items;
    directory->sector_pos = root_dir_sector_pos;
    directory->ending_sector_pos = root_dir_sector_pos + total_sectors;

    return 0;

read_error_out:
    kfree(dir);
error_out:
    return ret;
}

static int32_t fat16_resolve(struct disk *disk)
{
    struct fat_private_data *data = kmalloc(sizeof(*data));
    if (!data)
        return -ENOMEM;

    fat16_init_private_data(data, disk);

    disk->fs_private_data = data;
    disk->fs = &fat16_fs;

    int32_t ret = disk_stream_read(data->fat_read_stream, sizeof(data->header),
                                   (uint8_t *)&data->header);
    if (ret < 0)
        goto error_out;

    disk_stream_seek(data->fat_read_stream, 0);

    if (data->header.shared.extended_header.signature != 0x29) {
        ret = -EFSNOTUS;
        goto error_out;
    }

    ret = fat16_get_root_directory(disk, data, &data->root_directory);
    if (ret < 0) {
        ret = -EIO;
        goto error_out;
    }

    return 0;

error_out:
    kfree(data);
    return ret;
}

static void fat16_free_file_descriptor(struct fat_file_descriptor *descriptor)
{
    fat16_fat_item_free(descriptor->item);
    kfree(descriptor);
}

static int32_t fat16_close(void *descriptor)
{
    fat16_free_file_descriptor(descriptor);
    return 0;
}

static int32_t fat16_fstat(void *descriptor, struct file_stat *sb)
{
    struct fat_file_descriptor *desc = descriptor;
    struct fat_item *desc_item = desc->item;

    if (desc_item->type != FAT_ITEM_TYPE_FILE)
        return -EINVARG;

    struct fat_directory_item *item = desc_item->item;
    sb->file_size = item->file_size;
    sb->flags = 0;

    if (item->attribute & FAT_FILE_READ_ONLY)
        sb->flags |= FILE_STATE_READ_ONLY;

    return 0;
}

static int32_t fat16_seek(void *descriptor, uint32_t offset,
                          enum file_seek_mode whence)
{
    struct fat_file_descriptor *desc = descriptor;
    struct fat_item *desc_item = desc->item;

    if (desc_item->type != FAT_ITEM_TYPE_FILE)
        return -EINVARG;

    struct fat_directory_item *item = desc_item->item;
    if (offset >= item->file_size)
        return -EIO;

    switch (whence) {
    case FILE_SEEK_MODE_SET:
        desc->pos = offset;
        return 0;
    case FILE_SEEK_MODE_CUR:
        desc->pos += offset;
        return 0;
    case FILE_SEEK_MODE_END:
        return -EUNIMP;
    }

    return -EINVARG;
}

static int32_t fat16_read(struct disk *disk, void *descriptor, uint32_t size,
                          uint32_t nmemb, char *out_ptr)
{
    struct fat_file_descriptor *fat_desc = descriptor;
    struct fat_directory_item *item = fat_desc->item->item;

    uint32_t offset = fat_desc->pos;

    int32_t ret;
    for (uint32_t i = 0; i < nmemb; ++i) {
        ret = fat16_read_internal(disk, fat16_get_first_cluster(item), offset,
                                  size, out_ptr);
        if (ret < 0)
            goto out;

        out_ptr += size;
        offset += size;
    }

    ret = nmemb;
out:
    return ret;
}

struct fs *fat16_init(void)
{
    return &fat16_fs;
}
