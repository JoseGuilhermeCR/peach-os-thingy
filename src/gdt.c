#include "gdt.h"
#include "kernel.h"

// NOTE(José): Crazy guy wanted it to be written encodeGdtEntry even though
// every single function written until now has used snake_case. Is he blind or something? Seems like
// just copying somebody else's work.
// Also, note the beautiful way in which we use a random byte pointer instead of the proper
// struct gdt that we created! :^D I'm tired, just want to get this finished ffs.
static void encode_gdt_entry(uint8_t *target, struct gdt_structured source)
{
    if (source.limit > 0x10000 && (source.limit & 0xfff) != 0xfff) {
        panic("encode_gdt_entry: invalid argument\n");
    }

    target[6] = 0x40;
    if (source.limit > 0x10000) {
        source.limit >>= 12;
        target[6] = 0xc0;
    }

    target[0] = source.limit & 0xff;
    target[1] = (source.limit >> 8) & 0xff;
    target[6] |= (source.limit >> 16) & 0x0f;

    target[2] = source.base & 0xff;
    target[3] = (source.base >> 8) & 0xff;
    target[4] = (source.base >> 16) & 0xff;
    target[7] = (source.base >> 24) & 0xff;

    target[5] = source.type;
}

void gdt_structured_to_gdt(struct gdt *gdt,
                           const struct gdt_structured *gdt_structured,
                           uint32_t total_entries)
{
    for (uint32_t i = 0; i < total_entries; ++i) {
        encode_gdt_entry((uint8_t *)&gdt[i], gdt_structured[i]);
    }
}
