#include "file.h"

#include "disk.h"
#include "fat16.h"
#include "path.h"
#include "kernel.h"
#include "kheap.h"
#include "string.h"
#include "status.h"

#include <stddef.h>

#define MAX_FILE_SYSTEMS 2
#define MAX_FILE_DESCRIPTORS 512

static struct fs *file_systems[MAX_FILE_SYSTEMS];
static struct file_descriptor *file_descriptors[MAX_FILE_DESCRIPTORS];

static struct fs **get_free_fs(void)
{
    for (uint32_t i = 0; i < MAX_FILE_SYSTEMS; ++i) {
        if (!file_systems[i]) {
            return &file_systems[i];
        }
    }

    return NULL;
}

static int32_t allocate_file_descriptor(struct file_descriptor **fd_out)
{
    for (uint32_t i = 0; i < MAX_FILE_DESCRIPTORS; ++i) {
        if (file_descriptors[i])
            continue;

        struct file_descriptor *fd = kmalloc(sizeof(*fd));
        if (!fd)
            return -ENOMEM;

        fd->index = i + 1;

        file_descriptors[i] = fd;
        *fd_out = fd;
        return 0;
    }

    return -ENOMEM;
}

static struct file_descriptor *get_file_descriptor(uint32_t fd)
{
    if (fd >= MAX_FILE_SYSTEMS)
        return NULL;

    const uint32_t index = fd - 1;
    return file_descriptors[index];
}

static void file_free_descriptor(struct file_descriptor *descriptor)
{
    file_descriptors[descriptor->index - 1] = NULL;
    kfree(descriptor);
}

void fs_init(void)
{
    memset(file_systems, 0, sizeof(file_systems));
    memset(file_descriptors, 0, sizeof(file_descriptors));

    fs_add(fat16_init());
}

int32_t fs_add(struct fs *fs)
{
    struct fs **free = get_free_fs();
    if (!fs) {
        return -1;
    }

    *free = fs;
    return 0;
}

struct fs *fs_resolve(struct disk *disk)
{
    for (uint32_t i = 0; i < MAX_FILE_SYSTEMS; ++i) {
        if (!file_systems[i])
            continue;

        if (file_systems[i]->resolve(disk) == 0)
            return file_systems[i];
    }

    return NULL;
}

static uint32_t file_get_mode_by_string(const char *str)
{
    uint32_t mode = 0;
    switch (str[0]) {
    case 'r':
        mode = FILE_MODE_BIT_READ;
        break;
    case 'w':
        mode = FILE_MODE_BIT_WRITE;
        break;
    case 'a':
        mode = FILE_MODE_BIT_APPEND;
        break;
    }
    return mode;
}

int32_t fs_fopen(const char *pathname, const char *mode)
{
    int32_t ret = 0;
    struct path_root *root_path = path_parse_start(pathname, NULL);
    if (!root_path || !root_path->first) {
        ret = -EINVARG;
        goto out;
    }

    struct disk *disk = disk_get(root_path->drive_no);
    if (!disk || !disk->fs) {
        ret = -EIO;
        goto out;
    }

    uint32_t file_mode = file_get_mode_by_string(mode);
    if (file_mode == 0) {
        ret = -EINVARG;
        goto out;
    }

    void *descriptor_private_data =
        disk->fs->open(disk, root_path->first, file_mode);
    if (ISERR(descriptor_private_data)) {
        ret = ERROR_I(descriptor_private_data);
        goto out;
    }

    struct file_descriptor *desc = NULL;
    ret = allocate_file_descriptor(&desc);
    if (ret < 0)
        goto out;

    desc->fs = disk->fs;
    desc->data = descriptor_private_data;
    desc->disk = disk;

    ret = desc->index;

out:
    // FIXME(José): This guy is completely insane. Why would you set
    // the return value at alll if you are going to ignore it before returning?!?!?
    if (ret < 0)
        ret = 0;
    return ret;
}

int32_t fs_fstat(int32_t fd, struct file_stat *sb)
{
    struct file_descriptor *desc = get_file_descriptor(fd);
    if (!desc)
        return -EINVARG;
    return desc->fs->fstat(desc->data, sb);
}

int32_t fs_fclose(int32_t fd)
{
    struct file_descriptor *desc = get_file_descriptor(fd);
    if (!desc)
        return -EINVARG;

    const int32_t ret = desc->fs->close(desc->data);
    if (ret != 0)
        return ret;

    file_free_descriptor(desc);
    return 0;
}

int32_t fs_fseek(int32_t fd, int32_t offset, enum file_seek_mode whence)
{
    struct file_descriptor *desc = get_file_descriptor(fd);
    if (!desc)
        return -EINVARG;
    return desc->fs->seek(desc->data, offset, whence);
}

int32_t fs_fread(void *ptr, uint32_t size, uint32_t nmemb, int32_t fd)
{
    if (!ptr || size == 0 || nmemb == 0 || fd < -1)
        return -EINVARG;

    struct file_descriptor *desc = get_file_descriptor(fd);
    if (!desc)
        return -EINVARG;

    return desc->fs->read(desc->disk, desc->data, size, nmemb, (char *)ptr);
}
