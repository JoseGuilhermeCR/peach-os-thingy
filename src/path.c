// FIXME(José): Stop performing heap allocations here. The linked list itself isn't bad,
// but every node allocates a 4KiB page and we barely use the page itself so we are wasting
// a lot of memory here.

#include "path.h"

#include <stdbool.h>
#include <stdint.h>

#include "string.h"
#include "status.h"
#include "kheap.h"

#define PATH_PREFIX_LEN 3

static bool is_path_valid(const char *pathname)
{
    const size_t len = strlen(pathname);

    if (len < 3 || len > PEACHOS_MAX_PATH)
        return false;

    if (pathname[0] < '0' || pathname[0] > '9')
        return false;

    return pathname[1] == ':' && pathname[2] == PEACHOS_PATH_SEPARATOR;
}

static int32_t get_drive_number_from_pathname(const char **pathname)
{
    const char *p = *pathname;

    if (!is_path_valid(p))
        return -EBADPATH;

    const int32_t drive_no = *p - '0';

    // NOTE(José): Advance the path by it's prefix len.
    p += PATH_PREFIX_LEN;

    *pathname = p;
    return drive_no;
}

static struct path_root *create_root(int drive_no)
{
    struct path_root *r = kmalloc(sizeof(*r));
    if (!r)
        return NULL;

    r->drive_no = drive_no;
    r->first = NULL;
    return r;
}

static const char *get_path_part(const char **pathname)
{
    const char *p = *pathname;

    char *part = kmalloc(PEACHOS_MAX_PATH);

    uint32_t i = 0;
    while (*p && *p != PEACHOS_PATH_SEPARATOR && i < PEACHOS_MAX_PATH)
        part[i++] = *p++;
    part[i] = '\0';

    if (*p == PEACHOS_PATH_SEPARATOR)
        ++p;

    if (i == 0) {
        kfree(part);
        part = NULL;
    }

    *pathname = p;
    return part;
}

static struct path_part *path_parse_next_part(struct path_part *last,
                                              const char **pathname)
{
    const char *part_str = get_path_part(pathname);
    if (!part_str)
        return NULL;

    struct path_part *part = kmalloc(sizeof(*part));
    part->str = part_str;
    part->next = NULL;

    if (last)
        last->next = part;

    return part;
}

struct path_root *path_parse_start(const char *pathname,
                                   const char *current_dir_path)
{
    (void)current_dir_path;

    struct path_root *root = NULL;
    int32_t drive_no = -1;
    const char *tmp_pathname = pathname;

    if (strlen(tmp_pathname) > PEACHOS_MAX_PATH)
        return NULL;

    drive_no = get_drive_number_from_pathname(&tmp_pathname);
    if (drive_no < 0)
        goto out;

    root = create_root(drive_no);
    if (!root)
        goto out;

    struct path_part *part = path_parse_next_part(NULL, &tmp_pathname);
    if (!part)
        goto out;

    root->first = part;
    while (part)
        part = path_parse_next_part(part, &tmp_pathname);

out:
    return root;
}

void path_free(struct path_root *root)
{
    struct path_part *part = root->first;

    while (part) {
        struct path_part *next = part->next;
        kfree((void *)part->str);
        kfree(part);
        part = next;
    }

    kfree(root);
}
