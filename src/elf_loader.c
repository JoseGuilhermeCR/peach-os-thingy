#include "elf_loader.h"

#include "file.h"
#include "kheap.h"
#include <stdbool.h>

#include "task.h"
#include "status.h"
#include "string.h"

static const char elf_signature[] = { 0x7f, 'E', 'L', 'F' };

static bool elf_is_valid_signature(const void *buffer)
{
    return memcmp(buffer, (const void *)elf_signature, sizeof(elf_signature)) ==
           0;
}

static bool elf_is_valid_class(const struct elf_header *header)
{
    return header->e_ident[EI_CLASS] == ELF_CLASS_NONE ||
           header->e_ident[EI_CLASS] == ELF_CLASS_32;
}

static bool elf_is_valid_endianess(const struct elf_header *header)
{
    return header->e_ident[EI_DATA] == ELF_DATA_NONE ||
           header->e_ident[EI_DATA] == ELF_DATA_LITTLE_ENDIAN;
}

static bool elf_is_executable(const struct elf_header *header)
{
    return header->e_type == ET_EXEC &&
           header->e_entry >= PEACHOS_PROGRAM_VIRTUAL_ADDRESS;
}

static bool elf_has_program_header(const struct elf_header *header)
{
    return header->e_phoff != 0;
}

static void *elf_memory(struct elf_file *file)
{
    return file->data;
}

struct elf_header *elf_header(struct elf_file *file)
{
    return file->data;
}

struct elf32_shdr *elf_sheader(struct elf_header *header)
{
    return (struct elf32_shdr *)((elf32_addr)header + header->e_shoff);
}

struct elf32_phdr *elf_pheader(struct elf_header *header)
{
    if (!header->e_phoff)
        return NULL;

    return (struct elf32_phdr *)((elf32_addr)header + header->e_phoff);
}

static struct elf32_phdr *elf_program_header(struct elf_header *header,
                                             uint32_t idx)
{
    return &elf_pheader(header)[idx];
}

static struct elf32_shdr *elf_section(struct elf_header *header, uint32_t idx)
{
    return &elf_sheader(header)[idx];
}

static char *elf_str_table(struct elf_header *header)
{
    return (char *)header + elf_section(header, header->e_shstrndx)->sh_offset;
}

void *elf_virtual_base(struct elf_file *file)
{
    return file->vaddr;
}

void *elf_virtual_end(struct elf_file *file)
{
    return file->vaddr_end;
}

void *elf_phys_base(struct elf_file *file)
{
    return file->paddr;
}

void *elf_phys_end(struct elf_file *file)
{
    return file->paddr_end;
}

static int32_t elf_validate_loaded(struct elf_header *header)
{
    return (elf_is_valid_signature(header) && elf_is_valid_class(header) &&
            elf_is_valid_endianess(header) && elf_has_program_header(header)) ?
               0 :
               -EINFORMAT;
}

static int32_t elf_process_phdr_pt_load(struct elf_file *elf,
                                        struct elf32_phdr *phdr)
{
    if (elf->vaddr >= (void *)phdr->p_vaddr || !elf->vaddr) {
        elf->vaddr = (void *)phdr->p_vaddr;
        elf->paddr = elf_memory(elf) + phdr->p_offset;
    }

    const uint32_t vaddr_end = phdr->p_vaddr + phdr->p_filesz;
    if (elf->vaddr_end <= (void *)vaddr_end || !elf->vaddr_end) {
        elf->vaddr_end = (void *)vaddr_end;
        elf->paddr_end = elf_memory(elf) + phdr->p_offset + phdr->p_filesz;
    }

    return 0;
}

void *elf_phdr_phys_address(struct elf_file *elf, struct elf32_phdr *phdr)
{
    return elf_memory(elf) + phdr->p_offset;
}

static int32_t elf_process_pheader(struct elf_file *elf,
                                   struct elf32_phdr *phdr)
{
    if (phdr->p_type == PT_LOAD)
        return elf_process_phdr_pt_load(elf, phdr);
    return -1;
}

static int32_t elf_process_pheaders(struct elf_file *elf)
{
    struct elf_header *header = elf_header(elf);
    for (uint32_t i = 0; i < header->e_phnum; ++i) {
        struct elf32_phdr *phdr = elf_program_header(header, i);
        const int32_t ret = elf_process_pheader(elf, phdr);
        if (ret < 0)
            return ret;
    }

    return 0;
}

static int32_t elf_process_loaded(struct elf_file *elf)
{
    int32_t ret = 0;

    struct elf_header *header = elf_header(elf);
    ret = elf_validate_loaded(header);
    if (ret < 0)
        return ret;

    ret = elf_process_pheaders(elf);
    if (ret < 0)
        return ret;

    return ret;
}

int32_t elf_load(const char *filename, struct elf_file **elf_out)
{
    int32_t ret = 0;

    struct elf_file *elf = kzalloc(sizeof(*elf));
    if (!elf) {
        ret = -ENOMEM;
        goto elf_error;
    }

    int32_t fd = fs_fopen(filename, "r");
    if (fd < 0) {
        ret = fd;
        goto fopen_error;
    }

    struct file_stat sb;
    ret = fs_fstat(fd, &sb);
    if (ret < 0)
        goto stat_error;

    elf->data = kzalloc(sb.file_size);
    if (!elf->data) {
        ret = -ENOMEM;
        goto data_error;
    }

    ret = fs_fread(elf->data, sb.file_size, 1, fd);
    if (ret != 1)
        goto fread_error;

    fs_fclose(fd);

    ret = elf_process_loaded(elf);
    if (ret == 0) {
        *elf_out = elf;
        return 0;
    }

fread_error:
    kfree(elf->data);
data_error:
stat_error:
    fs_fclose(fd);
fopen_error:
    kfree(elf);
elf_error:
    return ret;
}

void elf_close(struct elf_file *elf)
{
    if (elf) {
        kfree(elf->data);
        kfree(elf);
    }
}
