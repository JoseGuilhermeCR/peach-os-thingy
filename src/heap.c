#include "heap.h"
#include "status.h"
#include "string.h"

#include <stdbool.h>

static int32_t validate_table(void *beg, void *end, struct heap_table *table)
{
    int32_t res = 0;

    const size_t table_size = (size_t)(end - beg);
    const size_t total_blocks = table_size / PEACHOS_HEAP_BLOCK_SIZE;

    if (table->entry_count != total_blocks) {
        res = -EINVARG;
        goto out;
    }

out:
    return res;
}

static bool is_ptr_block_aligned(void *ptr)
{
    return ((uintptr_t)ptr & (PEACHOS_HEAP_BLOCK_SIZE - 1)) == 0;
}

int32_t heap_create(struct heap *heap, void *beg, void *end,
                    struct heap_table *table)
{
    int32_t res = 0;

    if (!is_ptr_block_aligned(beg) || !is_ptr_block_aligned(end)) {
        res = -EINVARG;
        goto out;
    }

    memset(heap, 0, sizeof(*heap));
    heap->saddr = beg;
    heap->table = table;

    res = validate_table(beg, end, table);
    if (res < 0)
        goto out;

    memset(table->entries, HEAP_BLOCK_TABLE_ENTRY_FREE, table->entry_count);

    return 0;

out:
    return res;
}

static size_t align_to_upper(size_t size)
{
    const size_t non_aligned = size & (PEACHOS_HEAP_BLOCK_SIZE - 1);
    if (non_aligned == 0)
        return size;
    return size - non_aligned + PEACHOS_HEAP_BLOCK_SIZE;
}

static uint8_t heap_get_entry_type(uint8_t entry)
{
    return entry & 0x0f;
}

static int32_t heap_get_start_block(struct heap *heap, uint32_t total_blocks)
{
    struct heap_table *table = heap->table;

    int32_t bc = 0;
    int32_t bs = -1;

    for (size_t i = 0; i < table->entry_count; ++i) {
        if (heap_get_entry_type(table->entries[i]) !=
            HEAP_BLOCK_TABLE_ENTRY_FREE) {
            bc = 0;
            bs = -1;
            continue;
        }

        // TODO(José): Some kind of assert here in case we add
        // more HEAP_BLOCK_TABLE_ENTRY_*.

        if (bs == -1) {
            bs = i;
        }

        ++bc;
        if ((uint32_t)bc == total_blocks)
            break;
    }

    const int32_t ret[] = { -ENOMEM, bs };
    return ret[bs != -1];
}

static void *heap_block_to_address(struct heap *heap, uint32_t block)
{
    return heap->saddr + (block * PEACHOS_HEAP_BLOCK_SIZE);
}

static uint32_t heap_address_to_block(struct heap *heap, void *addr)
{
    return (uint32_t)(addr - heap->saddr) / PEACHOS_HEAP_BLOCK_SIZE;
}

static void heap_mark_blocks_taken(struct heap *heap, uint32_t start,
                                   uint32_t total)
{
    struct heap_table *table = heap->table;

    table->entries[start] = HEAP_BLOCK_TABLE_ENTRY_USED | HEAP_BLOCK_IS_FIRST;
    for (uint32_t i = 1; i < total; ++i) {
        const uint32_t cur = i + start;
        const uint32_t prev = cur - 1;

        table->entries[cur] = HEAP_BLOCK_TABLE_ENTRY_USED;
        table->entries[prev] |= HEAP_BLOCK_HAS_NEXT;
    }
}

static void heap_mark_blocks_free(struct heap *heap, uint32_t block)
{
    // TODO(José): Make sure the first block is indeed marked as IS_FIRST.
    struct heap_table *table = heap->table;

    for (uint32_t i = block; i < table->entry_count; ++i) {
        const uint8_t entry = table->entries[i];
        table->entries[i] = HEAP_BLOCK_TABLE_ENTRY_FREE;
        if (!(entry & HEAP_BLOCK_HAS_NEXT))
            break;
    }
}

static void *heap_malloc_blocks(struct heap *heap, uint32_t total_blocks)
{
    void *address = NULL;

    int32_t start_block = heap_get_start_block(heap, total_blocks);
    if (start_block < 0)
        goto out;

    address = heap_block_to_address(heap, start_block);

    // Mark the blocks as taken
    heap_mark_blocks_taken(heap, start_block, total_blocks);

out:
    return address;
}

void *heap_malloc(struct heap *heap, size_t size)
{
    const size_t aligned_size = align_to_upper(size);
    const uint32_t total_blocks = aligned_size / PEACHOS_HEAP_BLOCK_SIZE;
    return heap_malloc_blocks(heap, total_blocks);
}

void heap_free(struct heap *heap, void *ptr)
{
    heap_mark_blocks_free(heap, heap_address_to_block(heap, ptr));
}
