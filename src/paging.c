#include "paging.h"

#include <stdbool.h>

#include "kheap.h"
#include "status.h"

// NOTE(José): Inside paging.asm.
extern void paging_load_directory(uint32_t *directory);

static uint32_t *current_directory;

// NOTE(José): A 4gb linear address space.
struct paging_4gb_chunk *paging_new_4gb(uint16_t table_entry_flags,
                                        uint16_t directory_entry_flags)
{
    // NOTE(José): Make sure to mask any irrelevant bits of the flag
    // variables.
    table_entry_flags &= PTE_FLAGS_MASK;
    directory_entry_flags &= PTD_FLAGS_MASK;

    // NOTE(José): By default every directory entry is writeable.
    directory_entry_flags |= PTD_READ_WRITE;

    uint32_t *directory =
        kzalloc(sizeof(uint32_t) * PAGING_TOTAL_ENTRIES_PER_DIRECTORY);
    // TODO(José): handle failure.

    uint32_t offset = 0;
    for (uint32_t i = 0; i < PAGING_TOTAL_ENTRIES_PER_DIRECTORY; ++i) {
        uint32_t *table =
            kzalloc(sizeof(uint32_t) * PAGING_TOTAL_ENTRIES_PER_TABLE);
        // TODO(José): handle failure.
        for (uint32_t j = 0; j < PAGING_TOTAL_ENTRIES_PER_TABLE; ++j) {
            const uint32_t address = offset + j * PAGING_PAGE_SIZE;
            table[j] = address | table_entry_flags;
        }
        offset += PAGING_TOTAL_ENTRIES_PER_TABLE * PAGING_PAGE_SIZE;
        const uintptr_t address = (uintptr_t)table & ~0xfff;
        directory[i] = address | directory_entry_flags;
    }

    struct paging_4gb_chunk *chunk_4gb = kzalloc(sizeof(*chunk_4gb));
    // TODO(José): handle failure.
    chunk_4gb->dir_entry = directory;
    return chunk_4gb;
}

void paging_switch(const struct paging_4gb_chunk *chunk)
{
    paging_load_directory(chunk->dir_entry);
    current_directory = chunk->dir_entry;
}

uint32_t *paging_4gb_chunk_get_dir(struct paging_4gb_chunk *chunk)
{
    return chunk->dir_entry;
}

static bool is_page_aligned(void *ptr)
{
    return ((uintptr_t)ptr & (PAGING_PAGE_SIZE - 1)) == 0;
}

int32_t paging_map_range(const struct paging_4gb_chunk *chunk, void *virt,
                         void *phys, uint32_t count, uint16_t flags)
{
    int32_t ret = 0;
    for (uint32_t i = 0; i < count; ++i) {
        ret = paging_map(chunk, virt, phys, flags);

        if (ret < 0)
            break;

        virt += PAGING_PAGE_SIZE;
        phys += PAGING_PAGE_SIZE;
    }

    return ret;
}

int32_t paging_map_to(const struct paging_4gb_chunk *chunk, void *virt,
                      void *phys, void *phys_end, uint16_t flags)
{
    int32_t ret = 0;

    if ((uint32_t)virt % PAGING_PAGE_SIZE) {
        ret = -EINVARG;
        goto out;
    }

    if ((uint32_t)phys % PAGING_PAGE_SIZE) {
        ret = -EINVARG;
        goto out;
    }

    if ((uint32_t)phys_end < (uint32_t)phys) {
        ret = -EINVARG;
        goto out;
    }

    uint32_t total_bytes = phys_end - phys;
    uint32_t total_pages = total_bytes / PAGING_PAGE_SIZE;

    ret = paging_map_range(chunk, virt, phys, total_pages, flags);
out:
    return ret;
}

int32_t paging_map(const struct paging_4gb_chunk *chunk, void *vaddr,
                   void *addr, uint16_t flags)
{
    if (!is_page_aligned(vaddr) || !is_page_aligned(addr))
        return -EINVARG;

    flags &= PTE_FLAGS_MASK;

    const uintptr_t virtual_address = (uintptr_t)vaddr;

    // NOTE(José): Get the page directory entry which
    // contains the page table address for this virtual address.
    const uint32_t pde = chunk->dir_entry[virtual_address >> 22];

    uint32_t *pt = (uint32_t *)(pde & ~0xfff);
    // NOTE(José): Get the page table entry for this virtual
    // address and map it to the given physical address.
    pt[(virtual_address >> 12) & 0x3ff] = (uintptr_t)addr | flags;

    return 0;
}

void paging_free_4gb(struct paging_4gb_chunk *chunk)
{
    for (uint32_t i = 0; i < 1024; ++i) {
        uint32_t entry = chunk->dir_entry[i];
        uint32_t *table = (uint32_t *)(entry & ~0xfff);
        kfree(table);
    }
    kfree(chunk->dir_entry);
    kfree(chunk);
}

void *paging_align_address(void *ptr)
{
    uint32_t p = (uint32_t)ptr;

    const uint32_t unaligned = p % PAGING_PAGE_SIZE;
    if (unaligned) {
        p += PAGING_PAGE_SIZE - unaligned;
    }

    return (void *)p;
}

void *paging_align_to_lower_page(void *ptr)
{
    return (void *)((uint32_t)ptr - ((uint32_t)ptr % PAGING_PAGE_SIZE));
}

void paging_get(struct paging_4gb_chunk *chunk, void *vaddr, void **addr_out,
                uint16_t *flags_out)
{
    // NOTE(José) Align the address.
    const uintptr_t virtual_address = (uintptr_t)vaddr & ~0xfff;

    // NOTE(José): Get the page directory entry which
    // contains the page table address for this virtual address.
    const uint32_t pde = chunk->dir_entry[virtual_address >> 22];
    const uint32_t *pt = (uint32_t *)(pde & ~0xfff);

    // NOTE(José): Get the page table entry for this virtual
    // address and map it to the given physical address.
    uint32_t pte = pt[(virtual_address >> 12) & 0x3ff];

    *addr_out = (void *)(pte & ~0xfff);
    *flags_out = pte & PTE_FLAGS_MASK;
}

void *paging_get_physical_addr(struct paging_4gb_chunk *chunk, void *vaddr)
{
    void *vaddr_new = paging_align_to_lower_page(vaddr);
    void *diff = (void *)((uint32_t)vaddr - (uint32_t)vaddr_new);

    void *vaddr_out;
    uint16_t flags_out;
    paging_get(chunk, vaddr_new, &vaddr_out, &flags_out);
    return (void *)((uint32_t)vaddr_out + (uint32_t)diff);
}
