#include "kernel.h"
#include "file.h"
#include "idt.h"
#include "io.h"
#include "keyboard.h"
#include "kheap.h"
#include "paging.h"
#include "disk.h"
#include "disk_stream.h"
#include "path.h"
#include "string.h"
#include "gdt.h"
#include "tss.h"
#include "config.h"
#include "task.h"
#include "process.h"
#include "isr80h.h"

#include <stdint.h>
#include <stddef.h>

#define VGA_WIDTH 80
#define VGA_HEIGHT 20

static uint16_t *video_mem;
static uint16_t terminal_row;
static uint16_t terminal_col;

struct paging_4gb_chunk *kernel_chunk;

static uint16_t terminal_make_char(char c, uint8_t color)
{
    return (color << 8) | (uint8_t)c;
}

static void terminal_initialize(void)
{
    video_mem = (uint16_t *)0xB8000;
    terminal_row = 0;
    terminal_col = 0;

    for (uint32_t i = 0; i < VGA_WIDTH * VGA_HEIGHT; ++i)
        video_mem[i] = terminal_make_char(' ', 0);
}

static void terminal_put_char(uint32_t x, uint32_t y, char c, uint8_t color)
{
    video_mem[(y * VGA_WIDTH) + x] = terminal_make_char(c, color);
}

static void terminal_backspace();

static void terminal_write_char(char c, uint8_t color)
{
    if (c == '\n') {
        ++terminal_row;
        terminal_col = 0;
        return;
    }

    if (c == '\b') {
        terminal_backspace();
        return;
    }

    terminal_put_char(terminal_col, terminal_row, c, color);

    ++terminal_col;
    if (terminal_col >= VGA_WIDTH) {
        terminal_col = 0;
        ++terminal_row;
    }
}

static void terminal_backspace(void)
{
    if (!terminal_row && !terminal_col)
        return;

    if (!terminal_col) {
        --terminal_row;
        terminal_col = VGA_WIDTH;
    }

    --terminal_col;
    // FIXME(José): What the hell is this recursion like thing doing here.
    terminal_write_char(' ', 15);
    --terminal_col;
}

void print(const char *s)
{
    const size_t len = strlen(s);
    for (size_t i = 0; i < len; ++i) {
        terminal_write_char(s[i], 15);
    }
}

void putc(char c)
{
    terminal_write_char(c, 15);
}

void panic(const char *msg)
{
    asm("cli");
    print(msg);
    while (1) {
    }
}

static struct tss tss;

static struct gdt gdt_real[PEACHOS_TOTAL_GDT_SEGMENTS];

static struct gdt_structured gdt_structured[PEACHOS_TOTAL_GDT_SEGMENTS] = {
    {
        .base = 0x00,
        .limit = 0x00,
        .type = 0x00,
    },
    {
        .base = 0x00,
        .limit = 0xffffffff,
        .type = 0x9a, // Kernel Code Segment
    },
    {
        .base = 0x00,
        .limit = 0xffffffff,
        .type = 0x92, // Kernel Data Segment
    },
    {
        .base = 0x00,
        .limit = 0xffffffff,
        .type = 0xf8, // User Code Segment
    },
    {
        .base = 0x00,
        .limit = 0xffffffff,
        .type = 0xf2, // User Data Segment
    },
    {
        .base = (uint32_t)&tss,
        .limit = sizeof(tss),
        .type = 0xe9, // User Data Segment
    },
};

static void tss_setup(void)
{
    memset(&tss, 0, sizeof(tss));
    tss.esp0 = 0x600000;
    tss.ss0 = KERNEL_DATA_SELECTOR;
    tss_load(0x28);
}

static void gdt_setup(void)
{
    memset(gdt_real, 0, sizeof(gdt_real));
    gdt_structured_to_gdt(gdt_real, gdt_structured, PEACHOS_TOTAL_GDT_SEGMENTS);
    gdt_load(gdt_real, sizeof(gdt_real));
}

static void pic_timer_callback(const struct interrupt_frame *frame)
{
    print("pic_timer");
}

void kernel_page(void)
{
    kernel_registers();
    paging_switch(kernel_chunk);
}

void kernel_init(void)
{
    terminal_initialize();

    gdt_setup();

    kheap_init();

    fs_init();

    disk_search_and_init();

    idt_init();

    tss_setup();

    kernel_chunk =
        paging_new_4gb(PTE_READ_WRITE | PTE_PRESENT | PTE_USER_SUPERVISOR,
                       PTD_READ_WRITE | PTD_PRESENT | PTD_USER_SUPERVISOR);
    // TODO(José): handle failure.

    paging_switch(kernel_chunk);

    enable_paging();

    isr80h_register_commands();

    keyboard_init();

    // idt_register_interrupt_callback(0x20, pic_timer_callback);

    struct process *p = NULL;
    int32_t ret = process_load_switch("0:/blank", &p);
    if (ret != 0) {
        panic("Failed to load shell\n");
    }

    struct cmdarg arg;
    strcpy(arg.arg, "Testing!");
    arg.next = NULL;

    process_inject_arguments(p, &arg);

    ret = process_load_switch("0:/blank", &p);
    if (ret != 0) {
        panic("Failed to load shell\n");
    }

    strcpy(arg.arg, "Abc!");
    arg.next = NULL;
    process_inject_arguments(p, &arg);

    task_run_first_ever_task();

    while (1) {
    }
}
