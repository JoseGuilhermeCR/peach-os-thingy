#include "keyboard.h"
#include "status.h"
#include "process.h"
#include "ps2_keyboard.h"

static struct keyboard *keyboard_list_head;
static struct keyboard *keyboard_list_last;

static int32_t keyboard_insert(struct keyboard *keyboard)
{
    int32_t ret = 0;
    if (!keyboard || !keyboard->init) {
        ret = -EINVARG;
        goto out;
    }

    if (keyboard_list_last) {
        keyboard_list_last->next = keyboard;
        keyboard_list_last = keyboard;
    } else {
        keyboard_list_head = keyboard;
        keyboard_list_last = keyboard;
    }

    ret = keyboard->init();
out:
    return ret;
}

void keyboard_init(void)
{
    keyboard_insert(ps2_keyboard_init());
}

static uint32_t keyboard_get_tail_index(struct process *p)
{
    return p->kb_buffer.tail % sizeof(p->kb_buffer.buffer);
}

void keyboard_backspace(struct process *p)
{
    --p->kb_buffer.tail;
    p->kb_buffer.buffer[keyboard_get_tail_index(p)] = 0;
}

void keyboard_push(char c)
{
    if (!c)
        return;

    struct process *p = process_get_current();
    if (!p) {
        return;
    }

    p->kb_buffer.buffer[keyboard_get_tail_index(p)] = c;
    ++p->kb_buffer.tail;
}

char keyboard_pop(void)
{
    struct task *t = task_get_current();
    if (!t) {
        return '\0';
    }

    struct process *p = t->process;

    const uint32_t real_index = p->kb_buffer.head % sizeof(p->kb_buffer.buffer);

    const char c = p->kb_buffer.buffer[real_index];
    if (c) {
        p->kb_buffer.buffer[real_index] = 0;
        ++p->kb_buffer.head;
    }

    return c;
}

void keyboard_set_capslock(struct keyboard *kb, bool is_enabled)
{
    kb->is_capslock_enabled = is_enabled;
}

bool keyboard_get_capslock(struct keyboard *kb)
{
    return kb->is_capslock_enabled;
}
