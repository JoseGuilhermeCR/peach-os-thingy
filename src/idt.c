#include "idt.h"
#include "process.h"
#include "string.h"
#include "config.h"
#include "io.h"
#include "status.h"

// TODO(José): Remove!
#include "kernel.h"
#include "task.h"

extern void *interrupt_ptr_table[PEACHOS_TOTAL_INTERRUPTS];

// NOTE(José): Inside idt.asm.
extern void idt_load(struct idtr_desc *desc);
extern void int21h(void);
extern void no_interrupt(void);
extern void isr80h_wrapper(void);

#define IDT_PRESENT (1 << 7)
#define IDT_DPL(x) ((x & 0x03) << 5)
#define IDT_TYPE_TASK_GATE 0x05
#define IDT_TYPE_16_INTERRUPT_GATE 0x06
#define IDT_TYPE_16_TRAP_GATE 0x07
#define IDT_TYPE_32_INTERRUPT_GATE 0x0e
#define IDT_TYPE_32_TRAP_GATE 0x0f

#define MAX_ISR80H_COMMANDS 1024

static struct idt_desc idt_descriptors[PEACHOS_TOTAL_INTERRUPTS];
static struct idtr_desc idtr_descriptor;

static isr80h_command_fn isr80h_commands[MAX_ISR80H_COMMANDS];
static int_callback_fn interrupt_callbacks[PEACHOS_TOTAL_INTERRUPTS];

static void idt_zero(void)
{
    print("Divide by zero error\n");
}

static void idt_set(uint32_t interrupt, void *address)
{
    if (interrupt > PEACHOS_TOTAL_INTERRUPTS)
        return;

    struct idt_desc *desc = &idt_descriptors[interrupt];

    desc->offset_lo = ((uint32_t)address) & 0x0000ffff;
    desc->offset_hi = ((uint32_t)address) >> 16;

    desc->selector = KERNEL_CODE_SELECTOR;
    // FIXME(José): We don't want to hardcode the DPL to 3.
    desc->type_attr = IDT_PRESENT | IDT_DPL(3) | IDT_TYPE_32_INTERRUPT_GATE;

    desc->reserved = 0;
}

void idt_handle_exception(const struct interrupt_frame *frame)
{
    (void)frame;
    process_terminate(task_get_current()->process);
    task_next();
}

void idt_clock(const struct interrupt_frame *frame)
{
    (void)frame;
    outb(0x20, 0x20);
    task_next();
}

void idt_init(void)
{
    memset(idt_descriptors, 0, sizeof(idt_descriptors));

    idtr_descriptor.limit = sizeof(idt_descriptors) - 1;
    idtr_descriptor.base = (uint32_t)(&idt_descriptors[0]);

    for (uint32_t i = 0; i < PEACHOS_TOTAL_INTERRUPTS; ++i) {
        idt_set(i, interrupt_ptr_table[i]);
    }

    idt_set(0, idt_zero);
    idt_set(0x80, isr80h_wrapper);

    for (uint32_t i = 0; i < 0x20; ++i) {
        idt_register_interrupt_callback(i, idt_handle_exception);
    }

    idt_register_interrupt_callback(0x20, idt_clock);

    idt_load(&idtr_descriptor);
}

void no_interrupt_handler(void)
{
    outb(0x20, 0x20);
}

int32_t idt_register_interrupt_callback(uint32_t interrupt, int_callback_fn fn)
{
    if (interrupt >= PEACHOS_TOTAL_INTERRUPTS)
        return -EINVARG;

    interrupt_callbacks[interrupt] = fn;
    return 0;
}

void isr80h_register_command(int32_t command, isr80h_command_fn command_fn)
{
    if (command < 0 || command >= MAX_ISR80H_COMMANDS)
        panic("The command is out of bounds\n");

    if (isr80h_commands[command])
        panic("You are attempting to overwrite an existing command\n");

    isr80h_commands[command] = command_fn;
}

static void *isr80h_handle_command(int32_t command,
                                   const struct interrupt_frame *frame)
{
    if (command < 0 || command >= MAX_ISR80H_COMMANDS)
        return 0;

    const isr80h_command_fn command_fn = isr80h_commands[command];
    if (!command_fn)
        return 0;

    return command_fn(frame);
}

void *isr80h_handler(int32_t command, const struct interrupt_frame *frame)
{
    kernel_page();
    task_current_save_state(frame);

    void *ret = isr80h_handle_command(command, frame);

    task_page();
    return ret;
}

void interrupt_handler(uint32_t interrupt, struct interrupt_frame *frame)
{
    kernel_page();
    if (interrupt_callbacks[interrupt]) {
        task_current_save_state(frame);
        interrupt_callbacks[interrupt](frame);
    }
    task_page();
    outb(0x20, 0x20);
}
